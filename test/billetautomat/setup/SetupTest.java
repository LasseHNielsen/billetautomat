package billetautomat.setup;
import billetautomat.data.Data;
import billetautomat.data.GUI;
import billetautomat.setup.properties.PropertyHandler;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * SetupTest
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class SetupTest 
{

    /**
     * Test of loadProperties method, of class Setup.
     */
    @Before
    public void testLoadProperties() {
        System.out.println("loadProperties");
        boolean expResult = true;
        boolean result = Setup.loadProperties();
        assertEquals(expResult, result);
        assertEquals("testVal", PropertyHandler.get("test"));
    }
    
    /**
     * Test of loadImages method, of class Setup.
     */
    @Test
    public void testLoadImages() {
        System.out.println("loadImages");
        boolean expResult = true;
        boolean result = Setup.loadImages();
        assertEquals(expResult, result);
        assertNotNull(GUI.BANNER);
        assertNotNull(GUI.LOGO);
    }

    /**
     * Test of fetchPrices method, of class Setup.
     */
    @Test
    public void testFetchPrices() {
        System.out.println("fetchPrices");
        boolean expResult = true;
        boolean result = Setup.fetchPrices();
        assertEquals(expResult, result);
        assertTrue(Data.getPrice(2) > 0);
        assertTrue(Data.getPrice(3) > Data.getPrice(2));
        assertTrue(Data.getPrice(4) > Data.getPrice(3));
        assertTrue(Data.getPrice(5) > Data.getPrice(4));
        assertTrue(Data.getPrice(6) > Data.getPrice(5));
        assertTrue(Data.getPrice(7) > Data.getPrice(6));
        assertTrue(Data.getPrice(8) > Data.getPrice(7));
        assertTrue(Data.getPrice(9) > Data.getPrice(8));
    }
    
    /**
     * Test of createFrame method, of class Setup.
     */
    @After
    public void testCreateFrame() {
        System.out.println("createFrame");
        boolean expResult = true;
        boolean result = Setup.createFrame();
        assertEquals(expResult, result);
        assertNotNull(GUI.frame);
    }

    /**
     * Test of startActivityListener method, of class Setup.
     */
    @After
    public void testStartActivityListener() {
        System.out.println("startActivityListener");
        boolean expResult = true;
        boolean result = Setup.startActivityListener();
        assertEquals(expResult, result);
    }
}