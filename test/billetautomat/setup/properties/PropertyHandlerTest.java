package billetautomat.setup.properties;
import java.io.IOException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * PropertyHandlerTest
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class PropertyHandlerTest 
{
    @Before
    public void setUp() {
        try {
            PropertyHandler.loadProperties();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test of get method, of class PropertyHandler.
     */
    @Test
    public void testGet() {
        System.out.println("get");
        String key = "test";
        String expResult = "testVal";
        String result = PropertyHandler.get(key);
        assertEquals(expResult, result);
    }

    /**
     * Test of saveProperties method, of class PropertyHandler.
     */
    @After
    public void testSaveProperties() throws Exception {
        System.out.println("saveProperties");
        String stationName = PropertyHandler.get("stationName");
        int zone = Integer.parseInt(PropertyHandler.get("zone"));
        String serverIP = PropertyHandler.get("serverIP");
        int serverPort = Integer.parseInt(PropertyHandler.get("serverPort"));
        int frameWidth = Integer.parseInt(PropertyHandler.get("frameWidth"));
        int frameHeigth = Integer.parseInt(PropertyHandler.get("frameHeigth"));
        int loggerLevel = Integer.parseInt(PropertyHandler.get("loggerLevel"));
        PropertyHandler.saveProperties(stationName, zone, serverIP, serverPort, frameWidth, frameHeigth, loggerLevel);
    }
}