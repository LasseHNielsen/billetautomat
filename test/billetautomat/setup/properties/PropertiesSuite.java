package billetautomat.setup.properties;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * PropertiesSuite
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({billetautomat.setup.properties.PropertyHandlerTest.class})
public class PropertiesSuite
{
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
}