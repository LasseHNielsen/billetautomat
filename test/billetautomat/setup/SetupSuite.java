package billetautomat.setup;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * SetupSuite
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({billetautomat.setup.SetupTest.class,billetautomat.setup.properties.PropertiesSuite.class})
public class SetupSuite { }