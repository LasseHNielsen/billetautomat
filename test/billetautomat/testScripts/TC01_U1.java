package billetautomat.testScripts;
import billetautomat.data.Data;
import billetautomat.gui.brugerMenu.*;
import billetautomat.setup.Setup;
import java.util.HashMap;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * TC01_U1
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class TC01_U1 
{
    @Before
    public void setUp() {
        Data.setPrice(2, 24);
        assertEquals(24, Data.getPrice(2));
        
        Data.setPrice(3, 36);
        assertEquals(36, Data.getPrice(3));
        
        for(int zone = 4; zone < 10; zone++) {
            Data.setPrice(zone, 1);
            assertEquals(1, Data.getPrice(zone));
        }
        
        Setup.loadProperties();
        new BrugerMenuController();
    }
    
    @Test
    public void testTC01_U1() {
        BrugerMenuController.addOrder(2, 2);
        BrugerMenuController.addOrder(3, 1);
    }
    
    @After
    public void tearDown() {
        final HashMap<Integer, Integer> bestilling = BrugerMenuController.getOrder();
        System.out.println(bestilling.toString());
        
        assertEquals(new Integer(2), bestilling.get(2));
        assertEquals(new Integer(1), bestilling.get(3));
        
        for (int zone = 4; zone < 10; zone++) {
            assertEquals(new Integer(0), bestilling.get(zone));
        }
        
        assertEquals(84, BrugerMenuController.getCost());
    }
}