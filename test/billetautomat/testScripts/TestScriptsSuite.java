package billetautomat.testScripts;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * TestScriptsSuite
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({billetautomat.testScripts.TC01_U1.class, billetautomat.testScripts.TC02_U1.class, billetautomat.testScripts.TC03_U2.class})
public class TestScriptsSuite { }