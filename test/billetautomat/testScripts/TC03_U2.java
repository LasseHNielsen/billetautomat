package billetautomat.testScripts;
import billetautomat.gui.brugerMenu.payment.PaymentController;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * TC01_U1
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class TC03_U2 
{
    private PaymentController controller;
    
    @Before
    public void setUp() {
        controller = new PaymentController(48, 0, 0);
        controller.cashOut(0);
    }
    
    @Test
    public void testTC03_U2() {
        PaymentController.insertMoney(50);
    }
    
    @After
    public void tearDown() {
        assertEquals(50, controller.getInsertedMoney());
        assertEquals(2, controller.cashOut(48));
        assertEquals(0, controller.getInsertedMoney());
    }
}