package billetautomat.testScripts;
import billetautomat.data.Data;
import billetautomat.gui.brugerMenu.*;
import billetautomat.setup.Setup;
import java.util.HashMap;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * TC01_U1
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class TC02_U1 
{
    @Before
    public void setUp() {
        for(int zone = 2; zone < 10; zone++) {
            Data.setPrice(zone, 1);
            assertEquals(1, Data.getPrice(zone));
        }
        
        Data.setPrice(4, 48);
        assertEquals(48, Data.getPrice(4));
        
        Setup.loadProperties();
        new BrugerMenuController();
    }
    
    @Test
    public void testTC02_U1() {
        BrugerMenuController.addOrder(4, -1);
    }
    
    @After
    public void tearDown() {
        final HashMap<Integer, Integer> bestilling = BrugerMenuController.getOrder();
        System.out.println(bestilling.toString());
        
        assertEquals(new Integer(0), bestilling.get(4));
        
        for (int zone = 2; zone < 10; zone++) {
            assertEquals(new Integer(0), bestilling.get(zone));
        }
        
        assertEquals(0, BrugerMenuController.getCost());
    }
}