package billetautomat.client;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ClientSuite
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({billetautomat.client.ClientCommTest.class})
public class ClientSuite { }