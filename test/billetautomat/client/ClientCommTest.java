package billetautomat.client;
import billetautomat.setup.Setup;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 * ClientCommTest
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class ClientCommTest 
{
    @Before
    public void setUp() {
        Setup.loadProperties();
    }
    
    @Test
    public void testSendRequest() {
        System.out.println("sendRequest");
        String request = "test";
        String expResult = "testReply";
        String result = ClientComm.sendRequest(request);
        assertEquals(expResult, result);
    }
}