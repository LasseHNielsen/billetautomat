package billetautomat.gui.brugerMenu;
import billetautomat.data.Data;
import billetautomat.data.ResourceBundles;
import billetautomat.setup.Setup;
import java.util.HashMap;
import java.util.Locale;
import javax.swing.JButton;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * BrugerMenuModelTest
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class BrugerMenuModelTest 
{    
    @Before
    public void setUp() {
        Setup.loadProperties();
        
        for (int zone = 2; zone < 10; zone++) {
            Data.setPrice(zone, zone*10);
        }
    }

    /**
     * Test of addOrder method, of class BrugerMenuModel.
     */
    @Test
    public void testAddOrder() {
        System.out.println("addOrder");
        BrugerMenuModel instance = new BrugerMenuModel(new BrugerMenuController().getView());
        final int zone = 2;
        
        int value = 1;
        int expResult = 1;
        instance.addOrder(zone, value);
        assertEquals(new Integer(expResult), instance.getOrder().get(zone));
        
        instance.resetOrder();
        value = Integer.MAX_VALUE;
        expResult = Integer.MAX_VALUE;
        instance.addOrder(zone, value);
        assertEquals(new Integer(expResult), instance.getOrder().get(zone));
                
        instance.resetOrder();
        value = 0;
        expResult = 0;
        instance.addOrder(zone, value);
        assertEquals(new Integer(expResult), instance.getOrder().get(zone));
        
        instance.resetOrder();
        value = -1;
        expResult = 0;
        instance.addOrder(zone, value);
        assertEquals(new Integer(expResult), instance.getOrder().get(zone));
        
        instance.resetOrder();
        value = Integer.MIN_VALUE;
        expResult = 0;
        instance.addOrder(zone, value);
        assertEquals(new Integer(expResult), instance.getOrder().get(zone));
        
        instance.resetOrder();
        value = Integer.MAX_VALUE - 1;
        expResult = Integer.MAX_VALUE - 1;
        instance.addOrder(zone, value);
        value = 2;
        instance.addOrder(zone, value);
        assertEquals(new Integer(expResult), instance.getOrder().get(zone));
    }

    /**
     * Test of resetOrder method, of class BrugerMenuModel.
     */
    @Test
    public void testResetOrder() {
        System.out.println("resetOrder");
        BrugerMenuModel instance = new BrugerMenuModel(new BrugerMenuController().getView());
        
        for (int zone = 2; zone < 10; zone++) {
            instance.addOrder(zone, zone);
        }
        
        HashMap<Integer, Integer> order = instance.getOrder();
        for (int zone = 2; zone < 10; zone++) {
            assertEquals(new Integer(zone), order.get(zone));
        }
        
        instance.resetOrder();
        
        order = instance.getOrder();
        for (int zone = 2; zone < 10; zone++) {
            assertEquals(new Integer(0), order.get(zone));
        }
    }

    /**
     * Test of updateOrder method, of class BrugerMenuModel.
     */
    @Test
    public void testUpdateOrder() {
        System.out.println("updateOrder");
        BrugerMenuView view = new BrugerMenuView();
        BrugerMenuModel instance = new BrugerMenuModel(view);
        ResourceBundles.changeLocale(Locale.ENGLISH);
        
        instance.updateOrderTable();
        assertEquals("Your order:\n\n\nTotal: 0 kr.", view.getjTextAreaOrder().getText());
        
        for (int zone = 2; zone < 10; zone++) {
            instance.addOrder(zone, zone);
        }
        instance.updateOrderTable();
        assertEquals("Your order:\n\n"
                + "2 - 2 zones: 40 kr.\n"
                + "3 - 3 zones: 90 kr.\n"
                + "4 - 4 zones: 160 kr.\n"
                + "5 - 5 zones: 250 kr.\n"
                + "6 - 6 zones: 360 kr.\n"
                + "7 - 7 zones: 490 kr.\n"
                + "8 - 8 zones: 640 kr.\n"
                + "9 - 9 zones: 810 kr.\n\n"
                + "Total: 2.840 kr.", view.getjTextAreaOrder().getText());
    }

    /**
     * Test of freeze method, of class BrugerMenuModel.
     */
    @Test
    public void testFreeze() {
        System.out.println("freeze");
        BrugerMenuView view = new BrugerMenuView();
        BrugerMenuModel instance = new BrugerMenuModel(view);
        JButton[] addButtons = view.getZoneButtonsAdd();
        JButton[] subButtons = view.getZoneButtonsSub();
        
        assertTrue(view.getjButtonCancel().isEnabled());
        assertTrue(view.getjButtonReset().isEnabled());
        assertTrue(view.getjButtonPay().isEnabled());
        for (JButton button : addButtons) {
            assertTrue(button.isEnabled());
        }
        for (JButton button : subButtons) {
            assertTrue(button.isEnabled());
        }
        
        instance.freeze(true);
        
        assertFalse(view.getjButtonCancel().isEnabled());
        assertFalse(view.getjButtonReset().isEnabled());
        assertFalse(view.getjButtonPay().isEnabled());
        for (JButton button : addButtons) {
            assertFalse(button.isEnabled());
        }
        for (JButton button : subButtons) {
            assertFalse(button.isEnabled());
        }
        
        instance.freeze(false);
        
        assertTrue(view.getjButtonCancel().isEnabled());
        assertTrue(view.getjButtonReset().isEnabled());
        assertTrue(view.getjButtonPay().isEnabled());
        for (JButton button : addButtons) {
            assertTrue(button.isEnabled());
        }
        for (JButton button : subButtons) {
            assertTrue(button.isEnabled());
        }
    }

    /**
     * Test of getTotal method, of class BrugerMenuModel.
     */
    @Test
    public void testGetTotal() {
        System.out.println("getTotal");
        BrugerMenuModel instance = new BrugerMenuModel(new BrugerMenuView());
        int expResult = 80;
        
        instance.addOrder(2, 1);
        instance.addOrder(3, 2);
        instance.updateOrderTable();
        
        int result = instance.getTotal();
        assertEquals(expResult, result);
    }
}