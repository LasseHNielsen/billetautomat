package billetautomat.gui.brugerMenu;
import billetautomat.data.Data;
import billetautomat.setup.Setup;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.JPanel;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * BrugerMenuControllerTest
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class BrugerMenuControllerTest 
{
    private BrugerMenuController controller;
    
    @Before
    public void setUp() {
        Setup.loadProperties();
        
        for(int zone = 2; zone < 10; zone++) {
            Data.setPrice(zone, 0);
        }
        
        controller = new BrugerMenuController();
    }

    /**
     * Test of getView method, of class BrugerMenuController.
     */
    @Test
    public void testGetView() {
        System.out.println("getView");
        Object view = controller.getView();
        assertTrue(view instanceof JPanel);
    }

    /**
     * Test of getCost method, of class BrugerMenuController.
     */
    @Test
    public void testGetCost() {
        System.out.println("getCost");
        Data.setPrice(2, 10);
        BrugerMenuController.addOrder(2, 3);
        
        int expResult = 30;
        int result = BrugerMenuController.getCost();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of resetOrder method, of class BrugerMenuController.
     */
    @Test
    public void testResetOrder() {
        System.out.println("resetOrder");
        
        for (int zone = 2; zone < 10; zone++) {
            BrugerMenuController.addOrder(zone, zone);
        }
        
        HashMap<Integer, Integer> order = BrugerMenuController.getOrder();
        for (int zone = 2; zone < 10; zone++) {
            assertEquals(new Integer(zone), order.get(zone));
        }
        
        BrugerMenuController.resetOrder();
        
        order = BrugerMenuController.getOrder();
        for (int zone = 2; zone < 10; zone++) {
            assertEquals(new Integer(0), order.get(zone));
        }
    }

    /**
     * Test of addOrder method, of class BrugerMenuController.
     */
    @Test
    public void testAddOrder() {
        System.out.println("addOrder");
        BrugerMenuController.resetOrder();
        
        BrugerMenuController.addOrder(2, Integer.MIN_VALUE);
        BrugerMenuController.addOrder(3, -1);
        BrugerMenuController.addOrder(4, 0);
        BrugerMenuController.addOrder(5, 1);
        BrugerMenuController.addOrder(6, 2);
        BrugerMenuController.addOrder(7, 3);
        BrugerMenuController.addOrder(8, 4);
        BrugerMenuController.addOrder(9, Integer.MAX_VALUE);
        
        HashMap<Integer, Integer> order = BrugerMenuController.getOrder();
        assertEquals(new Integer(0), order.get(2));
        assertEquals(new Integer(0), order.get(3));
        assertEquals(new Integer(0), order.get(4));
        assertEquals(new Integer(1), order.get(5));
        assertEquals(new Integer(2), order.get(6));
        assertEquals(new Integer(3), order.get(7));
        assertEquals(new Integer(4), order.get(8));
        assertEquals(new Integer(Integer.MAX_VALUE), order.get(9));
    }

    /**
     * Test of freezeScreen method, of class BrugerMenuController.
     */
    @Test
    public void testFreezeScreen() {
        System.out.println("freezeScreen");
        
        BrugerMenuView view = controller.getView();
        JButton[] addButtons = view.getZoneButtonsAdd();
        JButton[] subButtons = view.getZoneButtonsSub();
        
        assertTrue(view.getjButtonCancel().isEnabled());
        assertTrue(view.getjButtonReset().isEnabled());
        assertTrue(view.getjButtonPay().isEnabled());
        for (JButton button : addButtons) {
            assertTrue(button.isEnabled());
        }
        for (JButton button : subButtons) {
            assertTrue(button.isEnabled());
        }
        
        BrugerMenuController.freezeScreen(true);
        
        assertFalse(view.getjButtonCancel().isEnabled());
        assertFalse(view.getjButtonReset().isEnabled());
        assertFalse(view.getjButtonPay().isEnabled());
        for (JButton button : addButtons) {
            assertFalse(button.isEnabled());
        }
        for (JButton button : subButtons) {
            assertFalse(button.isEnabled());
        }
        
        BrugerMenuController.freezeScreen(false);
        
        assertTrue(view.getjButtonCancel().isEnabled());
        assertTrue(view.getjButtonReset().isEnabled());
        assertTrue(view.getjButtonPay().isEnabled());
        for (JButton button : addButtons) {
            assertTrue(button.isEnabled());
        }
        for (JButton button : subButtons) {
            assertTrue(button.isEnabled());
        }
    }
}