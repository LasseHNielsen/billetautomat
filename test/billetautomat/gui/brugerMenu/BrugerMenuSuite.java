package billetautomat.gui.brugerMenu;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * BrugerMenuSuite
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({billetautomat.gui.brugerMenu.BrugerMenuViewTest.class,billetautomat.gui.brugerMenu.BrugerMenuModelTest.class,billetautomat.gui.brugerMenu.payment.PaymentSuite.class,billetautomat.gui.brugerMenu.BrugerMenuControllerTest.class})
public class BrugerMenuSuite { }