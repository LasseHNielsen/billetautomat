package billetautomat.gui.brugerMenu.payment;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * PaymentSuite
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({billetautomat.gui.brugerMenu.payment.GlassTest.class,billetautomat.gui.brugerMenu.payment.TicketTest.class,billetautomat.gui.brugerMenu.payment.PaymentControllerTest.class})
public class PaymentSuite { }