package billetautomat.gui.brugerMenu.payment;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * PaymentControllerTest
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class PaymentControllerTest
{
    @Before
    public void setUp() {
    }

    /**
     * Test of getInsertedMoney method, of class PaymentController.
     */
    @Test
    public void testSetGetInsertedMoney() {
        System.out.println("getAndSetInsertedMoney");
        PaymentController instance = new PaymentController(0, 0, 0);
        int expResult = 0;
        int result = instance.getInsertedMoney();
        assertEquals(expResult, result);
        
        expResult = 1;
        PaymentController.insertMoney(expResult);
        result = instance.getInsertedMoney();
        assertEquals(expResult, result);
        instance.cashOut(0);
        
        expResult = Integer.MAX_VALUE;
        PaymentController.insertMoney(expResult);
        result = instance.getInsertedMoney();
        assertEquals(expResult, result);
        instance.cashOut(0);
        
        expResult = Integer.MAX_VALUE;
        PaymentController.insertMoney(Integer.MAX_VALUE);
        PaymentController.insertMoney(1);
        result = instance.getInsertedMoney();
        assertEquals(expResult, result);
        instance.cashOut(0);
        
        expResult = 0;
        PaymentController.insertMoney(-1);
        result = instance.getInsertedMoney();
        assertEquals(expResult, result);
        instance.cashOut(0);
        
        expResult = 0;
        PaymentController.insertMoney(Integer.MIN_VALUE);
        result = instance.getInsertedMoney();
        assertEquals(expResult, result);
        instance.cashOut(0);
    }

    /**
     * Test of cashOut method, of class PaymentController.
     */
    @Test
    public void testCashOut() {
        System.out.println("cashOut");
        int cost = 0;
        PaymentController instance = new PaymentController(0, 0, 0);
        instance.cashOut(0);
        
        assertEquals(0, instance.cashOut(cost));
        
        PaymentController.insertMoney(50);
        assertEquals(50, instance.cashOut(cost));
        
        cost = 25;
        PaymentController.insertMoney(50);
        assertEquals(25, instance.cashOut(cost));
        
        cost = -25;
        PaymentController.insertMoney(-50);
        assertEquals(0, instance.cashOut(cost));
        
        cost = 0;
        PaymentController.insertMoney(-50);
        assertEquals(0, instance.cashOut(cost));
    }
}