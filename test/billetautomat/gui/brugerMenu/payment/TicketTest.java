package billetautomat.gui.brugerMenu.payment;
import billetautomat.setup.Setup;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * TicketTest
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class TicketTest
{
    @Before
    public void setUp() {
        Setup.loadProperties();
    }

    @Test
    public void testSomeMethod() {
        String station = "station";
        String zoner = "1";
        String dato = "28. Nov 2015 12:00";
        String pris = "24";
        
        Ticket ticket = new Ticket(station, zoner, dato, pris);
        
        assertEquals(station, ticket.getjLabelStation().getText());
        assertEquals(zoner, ticket.getjLabelZones().getText());
        assertEquals(dato, ticket.getjLabelDate().getText());
        assertEquals(pris, ticket.getjLabelPrice().getText());
    }
}