package billetautomat.gui.brugerMenu.payment;
import billetautomat.data.Data;
import java.awt.Point;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * GlassTest
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class GlassTest 
{    
    private Glass instance;

    /**
     * Test of setImage method, of class Glass.
     */
    @Before
    public void testSetImage() {
        System.out.println("setImage");
        instance = new Glass();
        Icon icon = new ImageIcon(getClass().getResource(Data.IMAGES_FILEPATH + "JetLogo.png"));
        instance.setImage(icon);
    }

    /**
     * Test of drawCoin method, of class Glass.
     */
    @Test
    public void testDrawCoin() {
        System.out.println("drawCoin");
        Point p = new Point(10, -10);
        instance.drawCoin(p);
        
    }

    /**
     * Test of getPoint method, of class Glass.
     */
    @After
    public void testGetPoint() {
        System.out.println("getPoint");
        Point expResult = new Point(10, -10);
        Point result = instance.getPoint();
        assertEquals(expResult, result);
    }
}