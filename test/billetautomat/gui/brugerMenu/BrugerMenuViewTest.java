package billetautomat.gui.brugerMenu;
import billetautomat.setup.Setup;
import java.awt.Graphics;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * BrugerMenuViewTest
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class BrugerMenuViewTest 
{
    @Before
    public void setUp() {
        Setup.loadProperties();
    }

    /**
     * Test of getZoneTitles method, of class BrugerMenuView.
     */
    @Test
    public void testGetZoneTitles() {
        System.out.println("getZoneTitles");
        BrugerMenuView instance = new BrugerMenuView();
        int expResult = 8;
        int result = instance.getZoneTitles().length;
        assertEquals(expResult, result);
    }

    /**
     * Test of getjButtonZoneMap method, of class BrugerMenuView.
     */
    @Test
    public void testGetjButtonZoneMap() {
        System.out.println("getjButtonZoneMap");
        BrugerMenuView instance = new BrugerMenuView();
        assertTrue(instance.getjButtonZoneMap() instanceof JButton);
    }

    /**
     * Test of getjButtonReset method, of class BrugerMenuView.
     */
    @Test
    public void testGetjButtonReset() {
        System.out.println("getjButtonReset");
        BrugerMenuView instance = new BrugerMenuView();
        JButton button = instance.getjButtonReset();
        assertTrue(button instanceof JButton);
        assertEquals("RESET", button.getActionCommand());
    }

    /**
     * Test of getjButtonCancel method, of class BrugerMenuView.
     */
    @Test
    public void testGetjButtonCancel() {
        System.out.println("getjButtonCancel");
        BrugerMenuView instance = new BrugerMenuView();
        JButton button = instance.getjButtonCancel();
        assertTrue(button instanceof JButton);
        assertEquals("CANCEL", button.getActionCommand());
    }

    /**
     * Test of getjButtonPay method, of class BrugerMenuView.
     */
    @Test
    public void testGetjButtonPay() {
        System.out.println("getjButtonPay");
        BrugerMenuView instance = new BrugerMenuView();
        JButton button = instance.getjButtonPay();
        assertTrue(button instanceof JButton);
        assertEquals("PAY", button.getActionCommand());
    }

    /**
     * Test of getjLabelInstructions method, of class BrugerMenuView.
     */
    @Test
    public void testGetjLabelInstructions() {
        System.out.println("getjLabelInstructions");
        BrugerMenuView instance = new BrugerMenuView();
        assertTrue(instance.getjLabelInstructions() instanceof JLabel);
    }

    /**
     * Test of getjLabelPriceTitle method, of class BrugerMenuView.
     */
    @Test
    public void testGetjLabelPriceTitle() {
        System.out.println("getjLabelPriceTitle");
        BrugerMenuView instance = new BrugerMenuView();
        assertTrue(instance.getjLabelPriceTitle() instanceof JLabel);
    }

    /**
     * Test of getjLabelQuantity method, of class BrugerMenuView.
     */
    @Test
    public void testGetjLabelQuantity() {
        System.out.println("getjLabelQuantity");
        BrugerMenuView instance = new BrugerMenuView();
        assertTrue(instance.getjLabelQuantity() instanceof JLabel);
    }

    /**
     * Test of getjLabelTitel method, of class BrugerMenuView.
     */
    @Test
    public void testGetjLabelTitel() {
        System.out.println("getjLabelTitel");
        BrugerMenuView instance = new BrugerMenuView();
        assertTrue(instance.getjLabelTitel() instanceof JLabel);
    }

    /**
     * Test of getZonePrices method, of class BrugerMenuView.
     */
    @Test
    public void testGetZonePrices() {
        System.out.println("getZonePrices");
        BrugerMenuView instance = new BrugerMenuView();
        int expResult = 8;
        int result = instance.getZonePrices().length;
        assertEquals(expResult, result);
    }

    /**
     * Test of getZoneQuantities method, of class BrugerMenuView.
     */
    @Test
    public void testGetZoneQuantities() {
        System.out.println("getZoneQuantities");
        BrugerMenuView instance = new BrugerMenuView();
        int expResult = 8;
        int result = instance.getZoneQuantities().length;
        assertEquals(expResult, result);
    }

    /**
     * Test of getZoneButtonsAdd method, of class BrugerMenuView.
     */
    @Test
    public void testGetZoneButtonsAdd() {
        System.out.println("getZoneButtonsAdd");
        BrugerMenuView instance = new BrugerMenuView();
        int expLength = 8;
        JButton[] result = instance.getZoneButtonsAdd();
        assertEquals(expLength, result.length);
        for (JButton button : result) {
            assertTrue(button instanceof JButton);
            assertTrue(button.getActionCommand().startsWith("ADD"));
        }
    }

    /**
     * Test of getZoneButtonsSub method, of class BrugerMenuView.
     */
    @Test
    public void testGetZoneButtonsSub() {
        System.out.println("getZoneButtonsSub");
        BrugerMenuView instance = new BrugerMenuView();
        int expLength = 8;
        JButton[] result = instance.getZoneButtonsSub();
        assertEquals(expLength, result.length);
        for (JButton button : result) {
            assertTrue(button instanceof JButton);
            assertTrue(button.getActionCommand().startsWith("SUB"));
        }
    }

    /**
     * Test of getjLabelOrderTitle method, of class BrugerMenuView.
     */
    @Test
    public void testGetjLabelOrderTitle() {
        System.out.println("getjLabelOrderTitle");
        BrugerMenuView instance = new BrugerMenuView();
        JLabel result = instance.getjLabelOrderTitle();
        assertTrue(result instanceof JLabel);
    }

    /**
     * Test of getjTextAreaOrder method, of class BrugerMenuView.
     */
    @Test
    public void testGetjTextAreaOrder() {
        System.out.println("getjTextAreaOrder");
        BrugerMenuView instance = new BrugerMenuView();
        JTextArea result = instance.getjTextAreaOrder();
        assertTrue(result instanceof JTextArea);
    }
}