package billetautomat.data;
import java.util.Locale;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * ResourceBundlesTest
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class ResourceBundlesTest 
{   
    /**
     * Test of changeLocale method, of class ResourceBundles.
     */
    @Test
    public void testChangeLocale() {
        System.out.println("changeLocale");
        
        Locale locale = Locale.FRANCE;
        ResourceBundles.changeLocale(locale);
        assertEquals("test_fr", ResourceBundles.getString("test"));
        
        locale = Locale.ENGLISH;
        ResourceBundles.changeLocale(locale);
        assertEquals("test_en", ResourceBundles.getString("test"));
    }

    /**
     * Test of getString method, of class ResourceBundles.
     */
    @Test
    public void testGetString_String() {
        System.out.println("getString");
        String key = "testSame";
        String expResult = "test";
        String result = ResourceBundles.getString(key);
        assertEquals(expResult, result);
    }

    /**
     * Test of getString method, of class ResourceBundles.
     */
    @Test
    public void testGetString_String_ObjectArr() {
        System.out.println("getString");
        String key = "testInsert";
        int params = -1;
        String expResult = "test"+params;
        String result = ResourceBundles.getString(key, params);
        assertEquals(expResult, result);
    }
}