package billetautomat.data;
import billetautomat.setup.properties.PropertyHandler;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * GUITest
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class GUITest 
{
    /**
     * Test of loadImages method, of class GUI.
     * @throws java.lang.Exception
     */
    @Test
    public void testLoadImages() throws Exception {
        System.out.println("loadImages");
        PropertyHandler.loadProperties();
        final GUI instance = new GUI();
        instance.loadImages();
        
        assertNotNull(GUI.BANNER);
        assertNotNull(GUI.LOGO);
    }
}