package billetautomat.data;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * DataTest
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class DataTest
{
    /**
     * Test of setPrice and getPrice methods, of class Data.
     */
    @Test
    public void testSetGetPrice() {
        System.out.println("setGetPrice");
        final int zone = 2;
        
        //Poisitiv
        int input = 0;
        int expResult = 0;
        Data.setPrice(zone, input);
        int result = Data.getPrice(zone);
        assertEquals(expResult, result);
        
        input = Integer.MAX_VALUE;
        expResult = Integer.MAX_VALUE;
        Data.setPrice(zone, input);
        result = Data.getPrice(zone);
        assertEquals(expResult, result);
        
        //Negativ        
        input = Integer.MIN_VALUE;
        expResult = 0;
        Data.setPrice(zone, input);
        result = Data.getPrice(zone);
        assertEquals(expResult, result);
        
        input = -1;
        expResult = 0;
        Data.setPrice(zone, input);
        result = Data.getPrice(zone);
        assertEquals(expResult, result);
    }
}