package billetautomat.data;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * DataSuite
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({billetautomat.data.GUITest.class,billetautomat.data.ResourceBundlesTest.class,billetautomat.data.DataTest.class})
public class DataSuite { }