package billetautomat.logger;
import java.util.logging.Level;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * LogTest
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class LogTest 
{
    @Before
    public void setUp() {
    }

    /**
     * Test of setLevel method, of class Log.
     */
    @Test
    public void testSetLevel() {
        System.out.println("setLevel");
        int input = Log.LEVEL_OFF;
        Level expResult = Level.OFF;
        Log.setLevel(input);
        Level result = Log.logger.getLevel();
        assertEquals(expResult, result);
        
        input = Log.LEVEL_SEVERE;
        expResult = Level.SEVERE;
        Log.setLevel(input);
        result = Log.logger.getLevel();
        assertEquals(expResult, result);
        
        input = Log.LEVEL_WARNING;
        expResult = Level.WARNING;
        Log.setLevel(input);
        result = Log.logger.getLevel();
        assertEquals(expResult, result);
        
        input = Log.LEVEL_CONFIG;
        expResult = Level.CONFIG;
        Log.setLevel(input);
        result = Log.logger.getLevel();
        assertEquals(expResult, result);
        
        input = Log.LEVEL_INFO;
        expResult = Level.INFO;
        Log.setLevel(input);
        result = Log.logger.getLevel();
        assertEquals(expResult, result);
        
        input = Log.LEVEL_FINE;
        expResult = Level.FINE;
        Log.setLevel(input);
        result = Log.logger.getLevel();
        assertEquals(expResult, result);
        
        input = Log.LEVEL_FINER;
        expResult = Level.FINER;
        Log.setLevel(input);
        result = Log.logger.getLevel();
        assertEquals(expResult, result);
        
        input = Log.LEVEL_FINEST;
        expResult = Level.FINEST;
        Log.setLevel(input);
        result = Log.logger.getLevel();
        assertEquals(expResult, result);
        
        input = Log.LEVEL_ALL;
        expResult = Level.ALL;
        Log.setLevel(input);
        result = Log.logger.getLevel();
        assertEquals(expResult, result);
    }
}