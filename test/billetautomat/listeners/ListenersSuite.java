package billetautomat.listeners;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ListenersSuite
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({billetautomat.listeners.ActivityListenerTest.class})
public class ListenersSuite { }