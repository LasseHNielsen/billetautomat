package billetautomat.listeners;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * ActivityListenerTest
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class ActivityListenerTest
{
    /**
     * Test of stop method, of class ActivityListener.
     */
    @Test
    public void testListener() {
        System.out.println("listener");
        ActivityListener instance = new ActivityListener();
        instance.start();
        
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        assertTrue(ActivityListener.getDelta() >= 100);
        
        ActivityListener.stop();
        
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long delta1 = ActivityListener.getDelta();
        
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long delta2 = ActivityListener.getDelta();
        
        assertTrue(delta1 == delta2);
    }
}