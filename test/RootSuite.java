import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * RootSuite
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({billetautomat.client.ClientSuite.class, billetautomat.data.DataSuite.class, billetautomat.listeners.ListenersSuite.class, billetautomat.logger.LoggerSuite.class, billetautomat.setup.SetupSuite.class, billetautomat.gui.brugerMenu.BrugerMenuSuite.class, billetautomat.testScripts.TestScriptsSuite.class})
public class RootSuite { }