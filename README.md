# # # # # # # # # README # # # # # # # # # # #

Jet Transport billetautomat version 0.1.


- - - - - - - - Om programmet - - - - - - - -

Dette program er skrevet til et studieprojekt i kursus 02315 Versionsstyring og testmetoder E15.

Programmets formål er at fungerer som en billetautomat, hvor man kan købe billetter til den
offentlige transport.

Programmet har stadig en del mangler og har derfor god mulighed for forbedringer.


- - - - - - - - - System krav - - - - - - - - -

 - Java Runtime Environment version 1.8 eller højere

 - Internet forbindelse

 - Computerskærm med mus eller touchscreen


- - - - - - Installation og opsætning - - - - -

Installation:

 1. Flyt .zip filen ind i den ønskede installationsmappe

 2. Udpak .zip filen i mappen

 3. Kør programmet ved at starte .jar filen

Konfiguration:
- Ved første opstart vil du blive bedt om at sætte indstillingerne i programmet

 3. Udfyld felterne i på skærmbilledet og tryk på knappen Gem

 3a.Pris serverens eksterne addresse er nasie.diskstation.me på port 4641


- - - - - Test og udviklingsværtøj - - - - -

Programmet er kodet og testet med JDK version 1.8.0_45 i Netbeans IDE 8.0.1
Unit test lavet med JUnit version 4.10
Pris serveren er kodet med JDK version 1.7 og kører på Synology diskstation DS213J (NAS)

Programmet er afprøvet på
OS: Windows 8.2 64-bit og Windows 7 64-bit, Intel core i7 - 2670QM 2.2 GHz.


- - - - - Udvikler og kontakt - - - - -

Programmet er udviklet af 
Lasse Holm Nielsen - S123954
lasse.holm.n@gmail.com