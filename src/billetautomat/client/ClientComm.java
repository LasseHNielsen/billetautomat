package billetautomat.client;
import billetautomat.logger.Log;
import billetautomat.setup.properties.PropertyHandler;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.logging.Level;

/**
 * ClientComm
 * Kommunikationspunkt til serveren
 * @author Lasse Holm Nielsen - S123954
 * @version 29-11-2015
 */
public class ClientComm 
{
    /**
     * Sender en anmodning til serveren og returnerer svaret.
     * @param request
     *      Anmodning til serveren
     * @return 
     *      Svar fra serveren
     */
    public static String sendRequest(String request) {
        try {
            try(final Socket s = new Socket(PropertyHandler.get("serverIP"), Integer.parseInt(PropertyHandler.get("serverPort")))) {
                final DataOutputStream outToServer = new DataOutputStream(s.getOutputStream());
                final BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
                
                outToServer.writeBytes(request + '\n');
                final String reply = in.readLine();
                
                outToServer.close();
                in.close();

                return reply;
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.logger.log(Level.SEVERE, "Was not able to retreive information from server: {0}", e.toString());
        }
        return null;
    }
}