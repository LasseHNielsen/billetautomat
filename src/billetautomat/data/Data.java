package billetautomat.data;
import java.util.HashMap;

/**
 * Data
 * Data klasse der indeholder generel data til brug i programmet
 * @author Lasse Holm Nielsen - S123954
 * @version 27-11-2015
 */
public class Data 
{
    public static final long INACTIVITY_TIME = 120000;                          //Inaktivitetstid i millisekunder før maskinen går tilbage til startskærmen
    public static final String PROPERTIES_FILEPATH = "bin/config.properties";   //Filstien til property filen
    public static final String LOG_FILEPATH = "bin/Log.log/";                   //Filstien til log filen
    public static final String RESOURCE_BUNDLE_FILEPATH = "billetautomat.data.lang.messages";   //Stien til resource bundle mappen (sprog filerne)
    public static final String IMAGES_FILEPATH = "/billetautomat/data/img/";    //Stien til mappen med billederne
    
    private static final HashMap<Integer, Integer> zonePrices = new HashMap();  //Prisliste
    
    /**
     * Sætter prisen for en zone
     * @param zone
     *      Zone som skal have sat prisen
     * @param price 
     *      Prisen som zonen skal have
     */
    public static void setPrice(int zone, int price) {
        int p = price;
        if(p < 0)
            p = 0;
        zonePrices.put(zone, p);
    }
    
    /**
     * Henter prisen for en zone
     * @param zone
     *      Zonen som prisen skal hentes for
     * @return 
     *      Zonens pris
     */
    public static int getPrice(int zone) {
        return zonePrices.get(zone);
    }
}