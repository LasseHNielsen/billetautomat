package billetautomat.data;
import billetautomat.gui.brugerMenu.BrugerMenuController;
import billetautomat.gui.brugerMenu.payment.PaymentController;
import billetautomat.setup.properties.PropertyHandler;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFrame;

/**
 * GUI
 * Data klasse der indeholder data som bruges primært af GUI'en
 * @author Lasse Holm Nielsen - S123954
 * @version 26-11-2015
 */
public class GUI 
{
    public static final Dimension GUI_DIMENSION = new Dimension(Integer.parseInt(PropertyHandler.get("frameWidth")), Integer.parseInt(PropertyHandler.get("frameHeigth"))); //Dimensionen for main framet
    public static final Color BACKGROUND_COLOR = new Color(40, 100, 150);       //Tema bagrundsfarven
    public static final Color FONT_COLOR = new Color(190, 225, 255);            //Tema font farven
    public static JFrame frame = new JFrame();                                  //Main framet
    public static BufferedImage LOGO;                                           //Logo billedet
    public static BufferedImage BANNER;                                         //Banner billedet
    
    public static final Font HEADER1_FONT = new Font("Tahoma", Font.BOLD, 30);  //Tema overskrift 1 font
    public static final Font HEADER2_FONT = new Font("Tahoma", Font.BOLD, 20);  //Tema overskrift 2 font
    public static final Font HEADER3_FONT = new Font("Tahoma", Font.BOLD, 18);  //Tema overskrift 3 font
    public static final Font TEXT_FONT = new Font("Tahoma", Font.PLAIN, 20);    //Tema brødtekst font
    public static final Font BUTTON_FONT = new Font("Tahoma", Font.BOLD, 16);   //Tema knap font
    
    /**
     * Henter og gemmer billeder
     * @throws IOException 
     */
    public void loadImages() throws IOException {
        LOGO = ImageIO.read(getClass().getResource(Data.IMAGES_FILEPATH + "JetLogo.png"));
        BANNER = ImageIO.read(getClass().getResource(Data.IMAGES_FILEPATH + "Banner.png"));
    }
    
    /**
     * Opdaterer teksten på panelerne.
     * Bruges generelt til skift af sprog
     */
    public static void updateMessages() {
        BrugerMenuController.updateMessages();
        PaymentController.updateLabels();
    }
}