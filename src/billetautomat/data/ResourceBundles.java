package billetautomat.data;
import billetautomat.logger.Log;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;

/**
 * ResourceBundles
 * Sprog klasse hvor strenge til GUI'en kan hentes fra. 
 * Klassen bruger ResourceBundles
 * @author Lasse Holm Nielsen - S123954
 * @version 19-11-2015
 */
public class ResourceBundles
{
    private static final String BUNDLE_NAME = Data.RESOURCE_BUNDLE_FILEPATH;    //Filstien til mappen for sprog filerne
    private static ResourceBundle resourceBundle;
    
    /**
     * Static konstruktør for klassen.
     * Henter sprog fil
     */
    static {
        try {
            resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);
        } catch (MissingResourceException e) {
            Log.logger.log(Level.SEVERE, "Could not find resource bundle file: {0}", e.toString());
        }
    }
    
    /**
     * Skifter sprog fil så andet sprog bruges
     * @param locale 
     *      Sprog der skal skiftes til
     */
    public static void changeLocale(Locale locale) {
        Locale loc = locale;
        
        if(loc == null)
            loc = Locale.getDefault();
        
        try {
            resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME, loc);
        } catch (MissingResourceException e) {
            Log.logger.log(Level.SEVERE, "Could not find resource bundle file: {0}", e.toString());
        }
    }
    
    /**
     * Henter teksten for den angivende nøgle
     * @param key
     *      Nøgle som bruges til at hente en betemt streng i sprog filen
     * @return 
     *      Teskten for den angivende nøgle
     */
    public static String getString(String key) {
        try {
            return resourceBundle.getString(key);
        } 
        catch (NullPointerException | MissingResourceException e) {
            System.err.println('\"' + key + "\" was not found as a message: " + e);
            Log.logger.log(Level.WARNING, "\"{0}\" was not found as a message: {1}", new Object[]{key, e});
            return "MISSING MESSAGE";
        }
    }
    
    /**
     * Henter teksten for den angivende nøgle og indsætter parameterne på de angivende pladser
     * @param key
     *      Nøgle som bruges til at hente en betemt streng i sprog filen
     * @param params
     *      Værdier som indsættes på nogle predeffienrede pladser i strengen
     * @return 
     *      Teskten for den angivende nøgle med de indsatte værdier
     */
    public static String getString(String key, Object... params) {
        try {
            return MessageFormat.format(resourceBundle.getString(key), params);
        } 
        catch (NullPointerException | MissingResourceException e) {
            System.err.println('\"' + key + "\" was not found as a message: " + e);
            Log.logger.log(Level.WARNING, "\"{0}\" was not found as a message: {1}", new Object[]{key, e});
            return "MISSING MESSAGE";
        }
    }
}