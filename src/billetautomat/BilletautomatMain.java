package billetautomat;
import billetautomat.setup.properties.*;
import billetautomat.setup.Setup;
import billetautomat.gui.PanelController;
import billetautomat.logger.Log;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import javax.swing.JFrame;

/**
 * BilletautomatMain
 * Kører programmet
 * @author Lasse Holm Nielsen - S123954
 * @version 26-11-2015
 */
public class BilletautomatMain 
{    
    /**
     * Main metode der kører programmet
     * @param args 
     *      Benyttes ikke
     */
    public static void main(String[] args) {
        //Starting program
        Log.logger.info("Starting program");
        
        //Loading properties
        Log.logger.info("Loading properties");
        boolean propertiesLoaded = false;
        while(!propertiesLoaded) {
            if(Setup.loadProperties()) {
                propertiesLoaded = true;
            }
            else {
                Log.logger.severe("Could not load properties");
                
                final JFrame propertyFrame = new JFrame("Missing properties");
                propertyFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                propertyFrame.setSize(600, 500);
                propertyFrame.setLocationRelativeTo(null);
                propertyFrame.setContentPane(new CreatePropertiesPanel("Could not load properties"));
                propertyFrame.setVisible(true);
                
                while(!CreatePropertiesPanel.isComplete()) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Log.logger.log(Level.WARNING, "{0}", e);
                    }
                }
                propertyFrame.dispatchEvent(new WindowEvent(propertyFrame, WindowEvent.WINDOW_CLOSING));
            }
        }
        
        //Setting logger level
        Log.logger.info("Setting logger level");
        try {
            final int loggerLevel = Integer.parseInt(PropertyHandler.get("loggerLevel"));
            Log.setLevel(loggerLevel);
            Log.logger.log(Level.FINE, "Logger level set to {0}", loggerLevel);
        } catch (Exception e) {
            e.printStackTrace();
            Log.logger.warning("Could not set logger level");
        }
        
        //Loading images
        Log.logger.info("Loading images");
        if(!Setup.loadImages())
            Log.logger.severe("Could not load images");
        
        //Fetching prices
        Log.logger.info("Fetching prices from server");
        if(!Setup.fetchPrices())
            Log.logger.severe("Could not load prices");
        
        //Creating frame
        Log.logger.info("Creating frame");
        if(!Setup.createFrame())
            Log.logger.severe("Could not setup frame");
        
        //Set activity listener
        Log.logger.info("Starting inactivity listener");
        if(!Setup.startActivityListener())
            Log.logger.warning("Could not start inactivity listener");
        
        //Go to start screen
        Log.logger.info("Go to start screen panel");
        try {
            PanelController.gotoPanel(PanelController.START_SCREEN);
        } catch (Exception e) {
            Log.logger.log(Level.SEVERE, "Could not apply start screen panel: {0}", e.toString());
        }
    }
}