package billetautomat.listeners;
import billetautomat.data.ResourceBundles;
import billetautomat.gui.PanelController;
import billetautomat.gui.brugerMenu.BrugerMenuController;
import billetautomat.gui.brugerMenu.payment.PaymentController;
import billetautomat.logger.Log;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * BrugerMenuButtonListener
 * Action listener som bruges til knapperne i bruger menu GUI'en
 * @author Lasse Holm Nielsen - S123954
 * @version 29-11-2015
 */
public class BrugerMenuButtonListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        ActivityListener.poke();
        final String ac = e.getActionCommand();
        
        if(ac.startsWith("ADD")) {
            BrugerMenuController.addOrder(Integer.parseInt(ac.substring(3)), 1);
        }
        else if(ac.startsWith("SUB")) {
            BrugerMenuController.addOrder(Integer.parseInt(ac.substring(3)), -1);
        }
        else {
            switch (ac) {
                case "RESET": {
                    BrugerMenuController.resetOrder(); break;
                }
                case "CANCEL": {
                    PanelController.gotoPanel(PanelController.START_SCREEN); break;
                }
                case "PAY": {
                    if(BrugerMenuController.getCost() > 0) {
                        BrugerMenuController.freezeScreen(true);
                    
                        final JFrame frame = new JFrame("Jet Transport - Coin Slot");
                        final PaymentController paymentController = new PaymentController(BrugerMenuController.getCost(), 685, 540);

                        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                        frame.setPreferredSize(new Dimension(690, 575));
                        frame.setLayout(new BorderLayout());
                        frame.add(paymentController.getLayeredPane(), BorderLayout.CENTER);
                        frame.pack();
                        frame.setLocationRelativeTo(null);
                        frame.setResizable(false);
                        frame.setVisible(true);

                        new PayedListener(paymentController, frame).start();
                    }
                    break;
                }
            }
        }
    }
}

/**
 * PayedListener
 * Undersøger om det fulde beløb er indsat i betallings GUI'en
 * @author Lasse Holm Nielsen - S123954
 * @version 29-11-2015
 */
class PayedListener implements Runnable 
{
    private final PaymentController controller;
    private final JFrame frame;
    private static boolean running;

    PayedListener(PaymentController controller, JFrame frame) {
        this.controller = controller;
        this.frame = frame;
    }
    
    public void start() {
        Thread thread = new Thread(this);
        thread.setDaemon(true);
        running = true;
        thread.start();
    }
    
    public void stop() {
        running = false;
    }

    @Override
    public void run() {
        int cost = BrugerMenuController.getCost();
        int insertedValue = 0;
        
        while(cost > insertedValue && running) {
            insertedValue = controller.getInsertedMoney();
            
            if(controller.isCanceled()) {
                cost = 0;
                break;
            }
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Log.logger.warning(ex.toString());
            }
        }
        
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
        if(cost > 0)
            BrugerMenuController.printTickets();
        JOptionPane.showMessageDialog(new JFrame(), ResourceBundles.getString("payment.cashOut", controller.cashOut(cost)), "Jet Transport - Change", JOptionPane.PLAIN_MESSAGE);
        
        PanelController.gotoPanel(PanelController.START_SCREEN);
    }
}