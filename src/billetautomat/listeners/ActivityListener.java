package billetautomat.listeners;
import billetautomat.data.Data;
import billetautomat.gui.PanelController;
import billetautomat.logger.Log;

/**
 * ActivityListener
 * Undersøger om der forestages noget på maskinen.
 * Hvis der ingen aktivitet er så går maskinen tilbage til startskærmen
 * @author Lasse Holm Nielsen - S123954
 * @version 27-11-2015
 */
public class ActivityListener implements Runnable
{
    private static long t0, t;
    private static boolean running = true, pause = false;
    
    /**
     * Starter listeneren
     */
    public void start() {
        Thread thread = new Thread(this);
        thread.setDaemon(true);
        poke();
        thread.start();
    }
    
    /**
     * Stopper listeneren
     */
    public static void stop() {
        running = false;
    }
    
    /**
     * Pauser listeneren
     * @param pause
     *      Sand hvis listeneren skal pauses og falsk når den skal starte igen
     */
    public static void pause(boolean pause) {
        ActivityListener.pause = pause;
    }
    
    /**
     * Angiver at der er aktivitet og derfor nulstilles timeren
     */
    public static void poke() {
        t0 = System.currentTimeMillis();
    }
    
    public static long getDelta() {
        return (t - t0);
    }

    @Override
    public void run() {
        while(running) {
            if(!pause)
                t = System.currentTimeMillis();
                if((t - t0) > Data.INACTIVITY_TIME) 
                    PanelController.gotoPanel(PanelController.START_SCREEN);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.err.println(e);
                Log.logger.info(e.getLocalizedMessage());
            }
        }
    }
}