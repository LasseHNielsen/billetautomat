package billetautomat.listeners;
import billetautomat.data.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Locale;
import javax.swing.JLabel;

/**
 * LanguageChosenListener
 * Mouse listener som undersøger om der trykkes på et af flagene i bruger menu GUI'en for at skifte sprog
 * @author Lasse Holm Nielsen - S123954
 * @version 27-11-2015
 */
public class LanguageChosenListener implements MouseListener
{
    @Override
    public void mouseClicked(MouseEvent e) { }

    @Override
    public void mousePressed(MouseEvent e) {
        ActivityListener.poke();
        final Object obj = e.getSource();
        if(obj instanceof JLabel) {
            final JLabel label = (JLabel) obj;
            Locale loc;
            switch (label.getName()) {
                default: {
                    loc = Locale.getDefault(); break;
                }
                case "langDa": {
                    loc = new Locale("da", "DK"); break;
                }
                case "langEn": {
                    loc = Locale.ENGLISH; break;
                }
                case "langDe": {
                    loc = Locale.GERMAN; break;
                }
                case "langFr": {
                    loc = Locale.FRENCH; break;
                }
            }
            ResourceBundles.changeLocale(loc);
            GUI.updateMessages();
        } }

    @Override
    public void mouseReleased(MouseEvent e) { }

    @Override
    public void mouseEntered(MouseEvent e) { }

    @Override
    public void mouseExited(MouseEvent e) { }
}