package billetautomat.listeners;
import billetautomat.gui.PanelController;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * StartScreenListener
 * Mouse listener der bruges på startskærmen.
 * Klassen undersøges om der klikkes på startskærmen for at komme videre til bruger menuen
 * @author Lasse Holm Nielsen - S123954
 * @version 27-11-2015
 */
public class StartScreenListener implements MouseListener 
{
    @Override
    public void mouseClicked(MouseEvent e) { }

    @Override
    public void mousePressed(MouseEvent e) {
        PanelController.gotoPanel(PanelController.USER_MENU);
    }

    @Override
    public void mouseReleased(MouseEvent e) { }

    @Override
    public void mouseEntered(MouseEvent e) { }

    @Override
    public void mouseExited(MouseEvent e) { }
}