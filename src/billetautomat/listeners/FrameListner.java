package billetautomat.listeners;
import billetautomat.logger.Log;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * FrameListner
 * Window listener som bruges på main framet
 * @author Lasse Holm Nielsen - S123954
 * @version 30-11-2015
 */
public class FrameListner implements WindowListener 
{
    @Override
    public void windowOpened(WindowEvent e) { }

    @Override
    public void windowClosing(WindowEvent e) {
        Log.logger.info("Program closing by frame\r\n");
    }

    @Override
    public void windowClosed(WindowEvent e) { }

    @Override
    public void windowIconified(WindowEvent e) { }

    @Override
    public void windowDeiconified(WindowEvent e) { }

    @Override
    public void windowActivated(WindowEvent e) { }

    @Override
    public void windowDeactivated(WindowEvent e) { }
}