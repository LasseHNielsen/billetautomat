package billetautomat.listeners;
import billetautomat.setup.properties.CreatePropertiesPanel;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JTextField;

/**
 * AdminInputListener
 * Key listener til text areas i admin GUI'en (manglende properties GUI inkl.).
 * Sørger for at korrekte datatyper indtastes i GUI'en og udfører "execute" metoden hvis der trykkes på tasten enter
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class AdminInputListener implements KeyListener 
{
    private final CreatePropertiesPanel view;                                   //Indtastnings panelet
    
    /**
     * Konstruktør for listeneren
     * @param view 
     *      Indtastningspanelet
     */
    public AdminInputListener(CreatePropertiesPanel view) {
        this.view = view;
    }
    
    @Override
    public void keyTyped(KeyEvent e) {
        final char c = e.getKeyChar();
        
        if(c == KeyEvent.VK_ENTER)
            view.execute();
        else {
            final Object obj = e.getSource();
            String ac = null;
            
            if(obj instanceof JTextField)      
                ac = ((JTextField) obj).getName();
            if(ac == null)
                ac = "";

            if(ac.equals("INT"))
                if(c == KeyEvent.VK_MINUS || !(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE)) 
                    e.consume();
        }
    }

    @Override
    public void keyPressed(KeyEvent e) { }

    @Override
    public void keyReleased(KeyEvent e) { }
}