package billetautomat.listeners;
import billetautomat.gui.brugerMenu.payment.Glass;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

/**
 * PaymentDragListener
 * Mouse motion listener som bruges i betallings GUI'en.
 * Klassen bruges til at tegne sen valgten mønt når den flyttes
 * @author Lasse Holm Nielsen - S123954
 * @version 29-11-2015
 */
public class PaymentDragListener implements MouseMotionListener
{
    private final Glass glass;

    public PaymentDragListener(Glass glass) {
        this.glass = glass;
    }
    
    @Override
    public void mouseDragged(MouseEvent e) {
        ActivityListener.poke();
        glass.drawCoin(e.getPoint());
    }

    @Override
    public void mouseMoved(MouseEvent e) { }
}