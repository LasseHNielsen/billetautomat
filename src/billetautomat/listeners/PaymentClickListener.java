package billetautomat.listeners;
import billetautomat.gui.brugerMenu.payment.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JLabel;

/**
 * PaymentClickListener
 * Mouse listener som bruges i betallings GUI'en.
 * Klassen undersøger hvilken mønt der vælges og om den slippes i betallingsfeltet
 * @author Lasse Holm Nielsen - S123954
 * @version 29-11-2015
 */
public class PaymentClickListener implements MouseListener
{
    private final Glass glass;
    private int selectedValue;

    public PaymentClickListener(Glass glass) {
        this.glass = glass;
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
        ActivityListener.poke();
        final JLabel label = Background.getSelctedImage(e.getPoint());
        
        if(label != null) {
            final String text = label.getText();
                    
            selectedValue = Integer.parseInt((text.substring(0, text.indexOf(" "))));
            glass.setImage(label.getIcon());
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        glass.setImage(null);
        
        if(PaymentController.isCoinInserted(e.getPoint()))
            PaymentController.insertMoney(selectedValue);
        selectedValue = 0;
    }
    
    @Override
    public void mouseClicked(MouseEvent e) { }

    @Override
    public void mouseEntered(MouseEvent e) { }

    @Override
    public void mouseExited(MouseEvent e) { }
}