package billetautomat.setup;
import billetautomat.listeners.StartScreenListener;
import billetautomat.listeners.ActivityListener;
import billetautomat.data.GUI;
import billetautomat.logger.Log;
import java.awt.*;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 * StartScreen
 * Startskærm GUI som vises når makinen ikke bruges
 * @author Lasse Holm Nielsen - S123954
 * @version 26-11-2015
 */
public class StartScreen extends JPanel implements Runnable
{
    private final Dimension backgroundDim;                                      //Dimensionen for panelet
    private static boolean running = true, pause = false;
    private long t0 = System.currentTimeMillis(), t = 0;
    private int lang = 0;                                                       //Angiver hvilket sprog der vises i rulleteksten
    
    /**
     * Konstruktør for startskærmen
     */
    public StartScreen() {
        ActivityListener.pause(true);
        this.addMouseListener(new StartScreenListener());
        this.backgroundDim = GUI.GUI_DIMENSION;
        this.setBounds(0, 0, backgroundDim.width, backgroundDim.height);
        Thread thread = new Thread(this);
        thread.setDaemon(true);
        thread.start();
    }
    
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        
        //Background
        int red, green, blue, redLim = 100, greenLim = 200, blueLim = 255;
        for(int i = 0; i < backgroundDim.height; i++) {
            red = redLim - (int) Math.round(i*(1.0*redLim/backgroundDim.height));
            green = greenLim - (int) Math.round(i*(1.0*greenLim/backgroundDim.height));
            blue = blueLim - (int) Math.round(i*(1.0*blueLim/backgroundDim.height));
            
            g.setColor(new Color(red, green, blue));
            g.drawLine(0, i, backgroundDim.width, i);
        }
        
        //Logo
        final BufferedImage logo = GUI.LOGO;
        g.drawImage(logo, backgroundDim.width/2-logo.getWidth()/2, backgroundDim.height-logo.getHeight()-50, null);
        
        //Title
        String text = "JET TRANSPORT";
        final int yAlignmentLine1 = 190;
        Font font = new Font("calibri", Font.BOLD, 50);
        FontMetrics fontMetrics = g.getFontMetrics(font);
        Dimension fontDim = new Dimension(fontMetrics.stringWidth(text), fontMetrics.getHeight());
        final int lineHeight = fontDim.height/10;
                        
        g.setFont(font);
        g.setColor(Color.WHITE);
        
        for(int i = yAlignmentLine1; i < yAlignmentLine1 + lineHeight; i++)
            g.drawLine(0, i, backgroundDim.width, i);
        
        final int yAlignmentLine2 = (int) (yAlignmentLine1 + fontDim.height*1.2);
        for(int i = yAlignmentLine2; i < yAlignmentLine2 + lineHeight; i++)
            g.drawLine(0, i, backgroundDim.width, i);
        
        final int yAlignmentTitle = (yAlignmentLine1 + lineHeight + yAlignmentLine2)/2 + fontDim.height/4;
        final int xAlignmentTitle = backgroundDim.width/2 - fontDim.width/2;
        g.drawString(text, xAlignmentTitle, yAlignmentTitle);
        
        //Rolling text
        final String[] rollingTexts = new String[] {"Tryk på skærmen for at bestille en billet", "Touch the screen to purchase a ticket", "Touchez l'écran pour acheter un billet", "Berühren Sie den Bildschirm, um ein Ticket zu erwerben"};
        if(lang >= rollingTexts.length)
            lang = 0;
        text = rollingTexts[lang];
        
        font = new Font("calibri", Font.BOLD, 25);
        fontMetrics = g.getFontMetrics(font);
        fontDim = new Dimension(fontMetrics.stringWidth(text), fontMetrics.getHeight());
        
        g.setFont(font);
        final int xAlignmentRol = (int) (t - t0)/5;
        if(xAlignmentRol > backgroundDim.width + fontDim.width) {
            t0 = System.currentTimeMillis();
            lang++;
        }
        
        g.drawString(text, backgroundDim.width - xAlignmentRol, yAlignmentLine2 + lineHeight + fontDim.height);
    }

    @Override
    public void run() {
        while(running) {
            if(!pause) {
                t = System.currentTimeMillis();
                this.repaint();
            }

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                System.err.println(e);
                Log.logger.warning(e.getLocalizedMessage());
            }
        }
    }
    
    /**
     * Pauser animationen på startskærmen
     * @param pause 
     *      Sand hvis animationen skal pauses
     */
    public static void pause(boolean pause) {
        StartScreen.pause = pause;
    }
    
    /**
     * 
     * Pauser animationen på startskærmen
     * @param running 
     *      Sand hvis animationen skal stoppes
     */
    public static void stop(boolean running) {
        StartScreen.running = running;
    }
}