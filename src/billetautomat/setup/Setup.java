package billetautomat.setup;
import billetautomat.client.ClientComm;
import billetautomat.data.*;
import billetautomat.listeners.ActivityListener;
import billetautomat.listeners.FrameListner;
import billetautomat.setup.properties.PropertyHandler;
import billetautomat.logger.Log;
import java.io.IOException;
import java.util.logging.Level;
import javax.swing.JFrame;

/**
 * Setup
 * Setup klassen som bruges ved opstart af programmet og har derfor en nær forbindelse til main klassen.
 * Klassen bruges til at hente og konstruere forskellige data
 * @author Lasse Holm Nielsen - S123954
 * @version 26-11-2015
 */
public class Setup 
{
    /**
     * Generer main framet
     * @return 
     *      Sand hvis operationen lykkes
     */
    public static boolean createFrame() {
        try {
            final JFrame frame = GUI.frame;
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setSize(GUI.GUI_DIMENSION.width + 5, GUI.GUI_DIMENSION.height + 10);
            frame.setLocationRelativeTo(null);
            frame.setResizable(false);
            frame.addWindowListener(new FrameListner());
            frame.setVisible(true);
            return true;
        } catch (Exception e) {
            System.err.println(e);
            Log.logger.warning(e.getLocalizedMessage());
            return false;
        }
    }
    
    /**
     * Indlæser billederne til GUI'en
     * @return 
     *      Sand hvis operationen lykkes
     */
    public static boolean loadImages() {
        try {
            final GUI gui = new GUI();
            gui.loadImages();
            return true;
        } catch (IOException | IllegalArgumentException e) {
            System.err.println(e);
            Log.logger.severe(e.toString());
            return false;
        }
    }
    
    /**
     * Indlæser property filen
     * @return 
     *      Sand hvis operationen lykkes
     */
    public static boolean loadProperties() {
        try {
            PropertyHandler.loadProperties();
            return true;
        } catch (IOException e) {
            System.err.println(e);
            Log.logger.warning(e.getLocalizedMessage());
            return false;
        }
    }
    
    /**
     * Henter billetpriserne fra serveren
     * @return 
     *      Sand hvis operationen lykkes
     */
    public static boolean fetchPrices() {
        try {
            String pricesString = ClientComm.sendRequest("getPrices");
            
            int zone = 2;
            while(pricesString.length() > 0) {
                if(zone < 10) {
                    int seperator = pricesString.indexOf(";");
                    
                    Data.setPrice(zone, Integer.parseInt(pricesString.substring(0, seperator)));
                    
                    pricesString = pricesString.substring(seperator+1);
                    zone++;
                }
                else {
                    Log.logger.warning("Price string from server has too many elements");
                    break;
                }
            }
            return true;
        } catch (Exception e) {
            Log.logger.log(Level.SEVERE, "Could not fetch prices: {0}", e.toString());
            return false;
        }
    }
    
    /**
     * Starter inactivitets listeneren, som undersøger om brugeren er inaktiv
     * @return 
     *      Sand hvis operationen lykkes
     */
    public static boolean startActivityListener() {
        try {
            new ActivityListener().start();
            return true;
        } catch (Exception e) {
            System.err.println(e);
            Log.logger.warning(e.getLocalizedMessage());
            return false;
        }
    }
}