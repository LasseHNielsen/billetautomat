package billetautomat.setup.properties;
import billetautomat.data.Data;
import billetautomat.logger.Log;
import java.io.*;
import java.util.Properties;
import java.util.logging.Level;

/**
 * PropertyHandler
 * @author Lasse
 * @version 28-11-2015
 */
public class PropertyHandler 
{
    private final static Properties prop = new Properties();
    
    /**
     * Indlæser properties fra property filen
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static void loadProperties() throws FileNotFoundException, IOException {
        try (InputStream input = new FileInputStream(Data.PROPERTIES_FILEPATH)) {
            prop.load(input);
        }
    }
    
    /**
     * Gemmer properties i en property fil (laver en ny hvis ingen findes)
     * @param stationName
     *      Stationens navn som maskinen står på
     * @param zone
     *      Zonen som maskinen er placeret i
     * @param serverIP
     *      Pris serverens IP addresse
     * @param serverPort
     *      Pris serverens port
     * @param frameWidth
     *      GUI'ens bredde
     * @param frameHeigth
     *      GUI'ens højde
     * @param loggerLevel
     *      Logger niveua
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static void saveProperties(String stationName, int zone, String serverIP, int serverPort, int frameWidth, int frameHeigth, int loggerLevel) throws FileNotFoundException, IOException {
        try (OutputStream output = new FileOutputStream(Data.PROPERTIES_FILEPATH)) {
            prop.setProperty("stationName", stationName);
            prop.setProperty("zone", ""+zone);
            prop.setProperty("serverIP", serverIP);
            prop.setProperty("serverPort", ""+serverPort);
            prop.setProperty("frameWidth", ""+frameWidth);
            prop.setProperty("frameHeigth", ""+frameHeigth);
            prop.setProperty("loggerLevel", ""+loggerLevel);
            prop.setProperty("test", "testVal");
            prop.store(output, "Machines local data");
            Log.logger.info("Properties saved");
        }
    }
    
    /**
     * Henter properties for den angivende nøgle
     * @param key
     *      Nøgle for den property der skal findes
     * @return 
     *      Den funde property
     */
    public static String get(String key) {
        final String property = prop.getProperty(key);
        
        if(property == null)
            Log.logger.log(Level.WARNING, "Property for key \"{0}\" is missing", key);
        
        return prop.getProperty(key);
    }
}