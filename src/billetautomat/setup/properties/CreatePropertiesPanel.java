package billetautomat.setup.properties;
import billetautomat.listeners.AdminInputListener;
import billetautomat.logger.Log;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.logging.Level;
import javax.swing.JOptionPane;

/**
 * CreatePropertiesPanel
 * GUI som vises hvis property filen ikke kan læses.
 * Her bedes administratoren om at indtaste de manglende oplysninger hvorefter de gemmes i en property fil til senere brug.
 * Denne GUI vil blive erstattet af administrator GUI'en når den bliver indført. GUI'en vil også blive beskyttet med en adgangskode
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class CreatePropertiesPanel extends javax.swing.JPanel 
{
    private static boolean complete;
    
    /**
     * Konstruktør for property panelet
     * @param message 
     *      Intruerende tekst
     */
    public CreatePropertiesPanel(String message) {
        initComponents();
        
        jLabelSubtitlle.setText(message);
        
        final KeyListener inputistener = new AdminInputListener(this);
        jTextFieldLocationStationInput.addKeyListener(inputistener);
        jTextFieldServerIPInput.addKeyListener(inputistener);
        jTextFieldFrameWidthInput.setName("INT");
        jTextFieldFrameWidthInput.addKeyListener(inputistener);
        jTextFieldFrameHeigthInput.setName("INT");
        jTextFieldFrameHeigthInput.addKeyListener(inputistener);
        jTextFieldLocationZoneInput.setName("INT");
        jTextFieldLocationZoneInput.addKeyListener(inputistener);
        jTextFieldServerPortInput.setName("INT");
        jTextFieldServerPortInput.addKeyListener(inputistener);
        jTextFieldLoggerLevelInput.setName("INT");
        jTextFieldLoggerLevelInput.addKeyListener(inputistener);
    }
    
    /**
     * Undersøger om panelet er færdigt med at blive brugt
     * @return 
     *      Sand hvis panelet er færdigt
     */
    public static boolean isComplete() {
        return complete;
    }
    
    /**
     * Gemmer de indtastede værdier i property filen (laver en ny fil hvis der ikke findes en)
     */
    public void execute() {
        try {
            final int frameWidth = Integer.parseInt(jTextFieldFrameWidthInput.getText());
            final int frameHeigth = Integer.parseInt(jTextFieldFrameHeigthInput.getText());
            final String stationName = jTextFieldLocationStationInput.getText();
            final int zone = Integer.parseInt(jTextFieldLocationZoneInput.getText());
            final String serverIP = jTextFieldServerIPInput.getText();
            final int serverPort = Integer.parseInt(jTextFieldServerPortInput.getText());
            final int loggerLevel = Integer.parseInt(jTextFieldLoggerLevelInput.getText());
            
            PropertyHandler.saveProperties(stationName, zone, serverIP, serverPort, frameWidth, frameHeigth, loggerLevel);
            complete = true;
        } catch (IOException | NumberFormatException e) {
            Log.logger.log(Level.CONFIG, "Could not save log: {0}", e.getLocalizedMessage());
            JOptionPane.showMessageDialog(null, "Could not save log:\n" + e);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelTitle = new javax.swing.JLabel();
        jLabelSubtitlle = new javax.swing.JLabel();
        jLabelFrameTitle = new javax.swing.JLabel();
        jLabelFrameWidthTitle = new javax.swing.JLabel();
        jTextFieldFrameWidthInput = new javax.swing.JTextField();
        jLabelFrameHeigthTitle = new javax.swing.JLabel();
        jTextFieldFrameHeigthInput = new javax.swing.JTextField();
        jLabelLocationTitle = new javax.swing.JLabel();
        jLabelLocationStationTitle = new javax.swing.JLabel();
        jTextFieldLocationStationInput = new javax.swing.JTextField();
        jLabelLocationZoneTitle = new javax.swing.JLabel();
        jTextFieldLocationZoneInput = new javax.swing.JTextField();
        jLabelServerTitle = new javax.swing.JLabel();
        jLabelServerIPTitle = new javax.swing.JLabel();
        jTextFieldServerIPInput = new javax.swing.JTextField();
        jLabelServerPortTitle = new javax.swing.JLabel();
        jTextFieldServerPortInput = new javax.swing.JTextField();
        jLabelLoggerTitel = new javax.swing.JLabel();
        jLabelLoggerLevelTitle = new javax.swing.JLabel();
        jTextFieldLoggerLevelInput = new javax.swing.JTextField();
        jButtonSave = new javax.swing.JButton();

        jLabelTitle.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabelTitle.setText("Create property file");

        jLabelSubtitlle.setFont(new java.awt.Font("Tahoma", 2, 18)); // NOI18N
        jLabelSubtitlle.setText("Message");

        jLabelFrameTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelFrameTitle.setText("Skærm");

        jLabelFrameWidthTitle.setText("Bredde");

        jTextFieldFrameWidthInput.setText("1085");

        jLabelFrameHeigthTitle.setText("Højde");

        jTextFieldFrameHeigthInput.setText("725");

        jLabelLocationTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelLocationTitle.setText("Lokation");

        jLabelLocationStationTitle.setText("Stations navn");

        jTextFieldLocationStationInput.setText("null");

        jLabelLocationZoneTitle.setText("Zone");

        jTextFieldLocationZoneInput.setText("0");

        jLabelServerTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelServerTitle.setText("Server");

        jLabelServerIPTitle.setText("IP-addresse");

        jTextFieldServerIPInput.setText("127.0.0.1");

        jLabelServerPortTitle.setText("Port");

        jTextFieldServerPortInput.setText("4641");

        jLabelLoggerTitel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelLoggerTitel.setText("Logger");

        jLabelLoggerLevelTitle.setText("Niveau");

        jTextFieldLoggerLevelInput.setText("3");

        jButtonSave.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonSave.setText("Gem");
        jButtonSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelFrameTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelLocationTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelServerTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelLoggerTitel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelSubtitlle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabelLoggerLevelTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextFieldLoggerLevelInput, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabelLocationStationTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelFrameWidthTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelServerIPTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextFieldServerIPInput, javax.swing.GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE)
                            .addComponent(jTextFieldFrameWidthInput)
                            .addComponent(jTextFieldLocationStationInput))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabelServerPortTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelLocationZoneTitle, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelFrameHeigthTitle, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldFrameHeigthInput)
                            .addComponent(jTextFieldLocationZoneInput)
                            .addComponent(jTextFieldServerPortInput)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButtonSave)))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabelFrameWidthTitle, jLabelLocationStationTitle, jLabelLoggerLevelTitle, jLabelServerIPTitle});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jTextFieldFrameWidthInput, jTextFieldLocationStationInput, jTextFieldLoggerLevelInput, jTextFieldServerIPInput});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabelFrameHeigthTitle, jLabelLocationZoneTitle, jLabelServerPortTitle});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelSubtitlle)
                .addGap(18, 18, 18)
                .addComponent(jLabelFrameTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelFrameWidthTitle)
                    .addComponent(jTextFieldFrameWidthInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelFrameHeigthTitle)
                    .addComponent(jTextFieldFrameHeigthInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabelLocationTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelLocationStationTitle)
                    .addComponent(jTextFieldLocationStationInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelLocationZoneTitle)
                    .addComponent(jTextFieldLocationZoneInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabelServerTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelServerIPTitle)
                    .addComponent(jTextFieldServerIPInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelServerPortTitle)
                    .addComponent(jTextFieldServerPortInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabelLoggerTitel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelLoggerLevelTitle)
                    .addComponent(jTextFieldLoggerLevelInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonSave)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveActionPerformed
        execute();
    }//GEN-LAST:event_jButtonSaveActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonSave;
    private javax.swing.JLabel jLabelFrameHeigthTitle;
    private javax.swing.JLabel jLabelFrameTitle;
    private javax.swing.JLabel jLabelFrameWidthTitle;
    private javax.swing.JLabel jLabelLocationStationTitle;
    private javax.swing.JLabel jLabelLocationTitle;
    private javax.swing.JLabel jLabelLocationZoneTitle;
    private javax.swing.JLabel jLabelLoggerLevelTitle;
    private javax.swing.JLabel jLabelLoggerTitel;
    private javax.swing.JLabel jLabelServerIPTitle;
    private javax.swing.JLabel jLabelServerPortTitle;
    private javax.swing.JLabel jLabelServerTitle;
    private javax.swing.JLabel jLabelSubtitlle;
    private javax.swing.JLabel jLabelTitle;
    private javax.swing.JTextField jTextFieldFrameHeigthInput;
    private javax.swing.JTextField jTextFieldFrameWidthInput;
    private javax.swing.JTextField jTextFieldLocationStationInput;
    private javax.swing.JTextField jTextFieldLocationZoneInput;
    private javax.swing.JTextField jTextFieldLoggerLevelInput;
    private javax.swing.JTextField jTextFieldServerIPInput;
    private javax.swing.JTextField jTextFieldServerPortInput;
    // End of variables declaration//GEN-END:variables
}