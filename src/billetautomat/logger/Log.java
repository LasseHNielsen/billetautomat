package billetautomat.logger;
import billetautomat.data.Data;
import java.util.logging.*;
import java.io.*;
import java.text.SimpleDateFormat;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * Log
 * Logger klasse som skriver events i programmet ned i en log fil og viser dem i terminalen.
 * @author Lasse Holm Nielsen - S123954
 * @version 28-11-2015
 */
public class Log 
{
    public static Logger logger;
    
    public static final int LEVEL_OFF = 0;
    public static final int LEVEL_SEVERE = 1;
    public static final int LEVEL_WARNING = 2;
    public static final int LEVEL_INFO = 3;
    public static final int LEVEL_CONFIG = 4;
    public static final int LEVEL_FINE = 5;
    public static final int LEVEL_FINER = 6;
    public static final int LEVEL_FINEST = 7;
    public static final int LEVEL_ALL = 8;

    /**
     * Static konstruktør for loggeren. Ændrer formattet
     */
    static {
        try {
            FileHandler fh = new FileHandler(Data.LOG_FILEPATH, true);
            
            fh.setFormatter(new Formatter() {
                @Override
                public String format(LogRecord rec) {
                    StringBuilder buf = new StringBuilder(1000);
                    buf.append(new SimpleDateFormat("d-M-yy HH:mm:ss").format(new java.util.Date()));
                    buf.append(": [");
                    buf.append(rec.getLevel());
                    buf.append("] ");
                    buf.append(formatMessage(rec));
                    buf.append("\r\n");
                    
                    try {
                        if(rec.getLevel().intValue() > Level.INFO.intValue())
                            JOptionPane.showMessageDialog(new JFrame(), rec.getLevel()+"\n"+formatMessage(rec), "Jet Transport - ERROR", JOptionPane.ERROR_MESSAGE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    
                    return buf.toString();
                    }
                });
            
            logger = Logger.getLogger("Log");
            logger.addHandler(fh);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Angiver hvilket nivau der skal logges.
     * @param level 
     *      Logger nivau (0=intet og 8=alt)
     */
    public static void setLevel(int level) {
        switch (level) {
            case 0: {
                logger.setLevel(Level.OFF); break;
            }
            case 1: {
                logger.setLevel(Level.SEVERE); break;
            }
            case 2: {
                logger.setLevel(Level.WARNING); break;
            }
            default:
            case 3: {
                logger.setLevel(Level.INFO); break;
            }
            case 4: {
                logger.setLevel(Level.CONFIG); break;
            }
            case 5: {
                logger.setLevel(Level.FINE); break;
            }
            case 6: {
                logger.setLevel(Level.FINER); break;
            }
            case 7: {
                logger.setLevel(Level.FINEST); break;
            }
            case 8: {
                logger.setLevel(Level.ALL); break;
            }
        }
    }
}