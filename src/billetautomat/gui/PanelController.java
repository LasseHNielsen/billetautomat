package billetautomat.gui;
import billetautomat.data.*;
import billetautomat.gui.brugerMenu.BrugerMenuController;
import billetautomat.listeners.ActivityListener;
import billetautomat.logger.Log;
import billetautomat.setup.StartScreen;
import java.util.logging.Level;
import javax.swing.JFrame;

/**
 * PanelController
 * Klassen bruges til at skifte mellem de forskellige panels i main framet
 * @author Lasse Holm Nielsen - S123954
 * @version 29-11-2015
 */
public class PanelController 
{
    public static final int START_SCREEN = 0;
    public static final int USER_MENU = 1;
    public static final int ADMIN_MENU = 2;
    
    /**
     * Skifter panel i main framet
     * @param panel 
     *      Panelet der skal skiftes til
     */
    public static void gotoPanel(int panel) {
        final JFrame frame = GUI.frame;
        frame.getContentPane().removeAll();
        
        switch(panel) {
            default:
            case 0: { //Start Screen
                frame.setTitle("Jet Transport - Welcome");
                StartScreen.pause(false);
                ActivityListener.pause(true);
                frame.getContentPane().add(new StartScreen());
                ResourceBundles.changeLocale(null);
                break;
            }
            case 1: { //User Menu
                frame.setTitle("Jet Transport - Menu");
                StartScreen.pause(true);
                frame.getContentPane().add(new BrugerMenuController().getView());
                ActivityListener.poke();
                ActivityListener.pause(false);
                break;
            }
            case 2: { //Admin Menu NOT IMPLEMENTED
                try {
                    throw new UnsupportedOperationException("Not supported yet.");
                } catch (UnsupportedOperationException e) {
                    Log.logger.log(Level.SEVERE, "Could not go to admin menu: {0}", e.toString());
                }
                break;
            }
        }
        
        GUI.frame.revalidate();
    }
}