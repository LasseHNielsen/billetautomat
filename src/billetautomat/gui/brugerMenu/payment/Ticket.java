package billetautomat.gui.brugerMenu.payment;
import billetautomat.data.Data;
import billetautomat.data.GUI;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * Ticket
 * Billet GUI
 * @author Lasse Holm Nielsen - S123954
 * @version 25-11-2015
 */
public class Ticket extends javax.swing.JPanel 
{
    /**
     * Konstruktør for en billet
     * @param localeInformation
     *      Maskinens lokale informationer (station og zone)
     * @param zones
     *      Antal zoner der er købt
     * @param date
     *      Hvornår billeten er købt
     * @param price 
     *      Billetens pris
     */
    public Ticket(String localeInformation, String zones, String date, String price) {
        initComponents();
        
        Font font = GUI.HEADER3_FONT;
        jLabelTitel.setFont(GUI.HEADER2_FONT);
        jLabelStationTitle.setFont(font);
        jLabelZonesTitle.setFont(font);
        jLabelDateTitle.setFont(font);
        jLabelPriceTitle.setFont(font);
        
        font = GUI.TEXT_FONT;
        jLabelStation.setFont(font);
        jLabelZones.setFont(font);
        jLabelDate.setFont(font);
        jLabelPrice.setFont(font);
        
        jLabelStation.setText(localeInformation);
        jLabelZones.setText(zones);
        jLabelDate.setText(date);
        jLabelPrice.setText(price);
        
        jLabelLogo.setIcon(new ImageIcon(getClass().getResource(Data.IMAGES_FILEPATH + "JetLogo.png")));
    }

    protected JLabel getjLabelDate() {
        return jLabelDate;
    }

    protected JLabel getjLabelPrice() {
        return jLabelPrice;
    }

    protected JLabel getjLabelStation() {
        return jLabelStation;
    }

    protected JLabel getjLabelZones() {
        return jLabelZones;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelTitel = new javax.swing.JLabel();
        jLabelStationTitle = new javax.swing.JLabel();
        jLabelStation = new javax.swing.JLabel();
        jLabelZonesTitle = new javax.swing.JLabel();
        jLabelZones = new javax.swing.JLabel();
        jLabelDateTitle = new javax.swing.JLabel();
        jLabelDate = new javax.swing.JLabel();
        jLabelLogo = new javax.swing.JLabel();
        jLabelPriceTitle = new javax.swing.JLabel();
        jLabelPrice = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabelTitel.setText("Jet Transport");

        jLabelStationTitle.setText("Station");

        jLabelZonesTitle.setText("Zoner");

        jLabelDateTitle.setText("Dato");

        jLabelPriceTitle.setText("Pris");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelLogo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelTitel)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelStationTitle)
                                    .addComponent(jLabelZonesTitle)
                                    .addComponent(jLabelDateTitle)
                                    .addComponent(jLabelPriceTitle)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabelStation)
                                            .addComponent(jLabelZones)
                                            .addComponent(jLabelDate)
                                            .addComponent(jLabelPrice))))))
                        .addGap(0, 300, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelTitel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelStationTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelStation)
                .addGap(18, 18, 18)
                .addComponent(jLabelZonesTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelZones)
                .addGap(18, 18, 18)
                .addComponent(jLabelDateTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelDate)
                .addGap(18, 18, 18)
                .addComponent(jLabelPriceTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelPrice)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 99, Short.MAX_VALUE)
                .addComponent(jLabelLogo)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelDate;
    private javax.swing.JLabel jLabelDateTitle;
    private javax.swing.JLabel jLabelLogo;
    private javax.swing.JLabel jLabelPrice;
    private javax.swing.JLabel jLabelPriceTitle;
    private javax.swing.JLabel jLabelStation;
    private javax.swing.JLabel jLabelStationTitle;
    private javax.swing.JLabel jLabelTitel;
    private javax.swing.JLabel jLabelZones;
    private javax.swing.JLabel jLabelZonesTitle;
    // End of variables declaration//GEN-END:variables
}