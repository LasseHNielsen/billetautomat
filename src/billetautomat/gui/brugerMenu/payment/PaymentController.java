package billetautomat.gui.brugerMenu.payment;
import billetautomat.data.ResourceBundles;
import billetautomat.listeners.PaymentDragListener;
import billetautomat.listeners.PaymentClickListener;
import java.awt.Point;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;

/**
 * PanelController
 * Controller til kontant betallings GUI'en
 * @author Lasse Holm Nielsen - S123954
 * @version 25-11-2015
 */
public class PaymentController
{
    private final JLayeredPane lpane = new JLayeredPane();                      //Layered pane som indeholder bagrundspanelet og forgrundspanelet
    private static Background canvass;                                          //Bagrundspanelet
    private final Glass glass = new Glass();                                    //Forgrundspanelet
    private static int cost, insertedMoney = 0;                                 //Værdien som skal betales og den indsatte værdi

    /**
     * Konstruktør for betallings GUI'en
     * @param cost
     *      Beløbbet der skal betales
     * @param width
     *      GUI'ens bredde
     * @param height 
     *      GUI'ens højde
     */
    public PaymentController(int cost, int width, int height) {
        canvass = new Background();
        PaymentController.cost = cost;
        canvass.updateLabels(cost, insertedMoney);
        canvass.setBounds(0, 0, width, height);
        canvass.addMouseListener(new PaymentClickListener(glass));
        canvass.addMouseMotionListener(new PaymentDragListener(glass));
        
        glass.setBounds(0, 0, width, height);
        glass.setOpaque(false);
        lpane.add(canvass, 0, 0);
        lpane.add(glass, 1, 0);
    }
    
    /**
     * Returnerer det samelede panel for betallings GUI'en
     * @return 
     *      Layered pane for betallings GUI'en
     */
    public JLayeredPane getLayeredPane() {
        return lpane;
    }
    
    /**
     * Returnerer det totale indbetalte beløb
     * @return 
     *      Indbetalte beløb
     */
    public int getInsertedMoney() {
        return insertedMoney;
    }
    
    /**
     * Indsætter penge i maskinen
     * @param value 
     *      Beløb der skal indsættes
     */
    public static void insertMoney(int value) {
        boolean overflow = false;
        
        try {
            Math.addExact(insertedMoney, value);
            Math.subtractExact(insertedMoney, value);
        } catch (ArithmeticException e) {
            overflow = true;
        }
        
        if(value > 0 && !overflow) {
            insertedMoney += value;
            canvass.setInsertedValue(insertedMoney);
        }
    }
    
    /**
     * Undersøger om mønten blev indsat i maskinen
     * @param p
     *      Punktet hvor mønten blev forsøgt indsat
     * @return 
     *      Sand hvis mønten blev indsat
     */
    public static boolean isCoinInserted(Point p) {
        return canvass.isCoinInserted(p);
    }

    /**
     * Undersøger om betallingen er blevet annuleret
     * @return 
     *      Sand hvis betallingen blev annuleret
     */
    public boolean isCanceled() {
        return canvass.isCanceled();
    }
    
    /**
     * Opdaterer teksten på GUI'en. Bruges når der skiftes sprog
     */
    public static void updateLabels() {
        if(canvass != null)
            canvass.updateLabels(cost, insertedMoney);
    }

    /**
     * Returnerer byttepenge og afslutter betallingen
     * @param cost 
     *      Den beløb der skal betales
     * @return 
     * Byttepenge
     */
    public int cashOut(int cost) {
        int price = cost;
        
        if(price < 0)
            price = 0;
        
        insertedMoney -= price;
        int change = insertedMoney;
        
        insertedMoney = 0;
        canvass.setInsertedValue(0);
        
        return change;
    }
}