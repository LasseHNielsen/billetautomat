package billetautomat.gui.brugerMenu.payment;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;
import javax.swing.Icon;
import javax.swing.JPanel;

/**
 * Glass
 * Øverste panel lag i kontant betallings GUI'en.
 * Bruges til at tegne animation
 * @author Lasse Holm Nielsen - S123954
 * @version 24-11-2015
 */
public class Glass extends JPanel
{
    private BufferedImage image;                                                //Billede af mønt der trækkes
    private Point p;                                                            //Punkt hvor mønten sidst blev tegnet
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        if(p != null && image != null)
            g.drawImage(image, p.x-(image.getWidth()/2), p.y-(image.getHeight()/2), null);
    }
    
    /**
     * Sætter billedet af mønten som trækkes
     * @param icon 
     *      Billede af den valgte mønt
     */
    public void setImage(Icon icon) {
        if(icon == null) {
            image = null;
            this.repaint();
        }
        else {
            image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TRANSLUCENT);
            icon.paintIcon(new JPanel(), image.getGraphics(), 0, 0);
        }
    }
    
    /**
     * Tegn den aktuelle mønt
     * @param p 
     *      Punktet hvor mønten skal tegnes
     */
    public void drawCoin(Point p) {
        if(image != null) {
            this.p = p;
            this.repaint();
        }
    }
    
    /**
     * Henter det punkt hvor mønten sidst blev tegnet
     * @return 
     *      Punkt på GUI'en
     */
    public Point getPoint() {
        return p;
    }
}