package billetautomat.gui.brugerMenu.payment;
import billetautomat.data.*;
import java.awt.Point;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * Background
 * Nederste panel lag i kontant betallings GUI'en.
 * Bruges til de faste elementer
 * @author Lasse Holm Nielsen - S123954
 * @version 24-11-2015
 */
public class Background extends javax.swing.JPanel 
{
    private static JLabel[] coinLabels;                                         //Label til billeder af de forskellige mønter
    private boolean cancel;

    /**
     * Konstruktør for nederste lag i betallings GUI'en. Bruges til de faste elementer
     */
    protected Background() {
        initComponents();
        
        //Coin Slot Icon
        final ImageIcon icon = new ImageIcon(getClass().getResource(Data.IMAGES_FILEPATH + "Coin slot.png"));
        jLabelCoinSlot.setIcon(icon);
        jLabelCoinSlot.setSize(icon.getIconWidth(), icon.getIconHeight());
        
        //Coin Icons
        coinLabels = new JLabel[]{jLabelCoin1, jLabelCoin2, jLabelCoin3, jLabelCoin4, jLabelCoin5};        
        setCoin(0, 1, "1kr.png");
        setCoin(1, 2, "2kr.png");
        setCoin(2, 5, "5kr.png");
        setCoin(3, 10, "10kr.png");
        setCoin(4, 20, "20kr.png");
    }
    
    /**
     * Sætter værdierne for de labels der indeholder billeder af mønter
     * @param index
     *      Indeks for møntet (0 for første mønt plads og 4 for sidste plads)
     * @param value
     *      Møntens værdie (2 kr., 5 kr. osv.)
     * @param filname 
     *      Billedets filnavn (2kr.png, 5kr.png osv.)
     */
    private void setCoin(int index, int value, String filname) {
        final JLabel label = coinLabels[index];
        final ImageIcon icon = new ImageIcon(getClass().getResource(Data.IMAGES_FILEPATH + filname));
        
        label.setText(value + " kr");
        label.setIcon(icon);
        label.setSize(icon.getIconWidth(), icon.getIconHeight());
    }
    
    /**
     * Returnerer det label som punktet ligger i
     * @param p
     *      Punkt på GUI'en som skal undersøges
     * @return 
     *      Det label som punktet ligger i. Null hvis punktet ikke ligger i noget label
     */
    public static JLabel getSelctedImage(Point p) {
        for (JLabel label : coinLabels) {
            if(label.getBounds().contains(p)) 
                return label;
        }
        return null;
    }
    
    /**
     * Undersøger om punktet ligger i dropzonen for mønterne
     * @param p
     *      Punkt på GUI'en som skal undersøges
     * @return 
     *      Sand hvis punktet ligger i dropzonen
     */
    public boolean isCoinInserted(Point p) {
        return jLabelCoinSlot.contains(p);
    }
    
    /**
     * Undersøger om betallingen er annuleret
     * @return 
     *      Sand hvis betallingen er blevet annuleret
     */
    protected boolean isCanceled() {
        return cancel;
    }
    
    /**
     * Sætter teksten på displayet så den totale indsatte værdi vises
     * @param insertedMoney 
     *      Den totale indsatte værdi
     */
    protected void setInsertedValue(int insertedMoney) {
        if(!cancel)
            jLabelLine2.setText(ResourceBundles.getString("payment.line2", insertedMoney));
    }
    
    /**
     * Opdaterer teksten på panelet. Bruges ved skift af sprog
     * @param cost
     *      Den samlede værdi ser skal indbetales
     * @param inserted 
     *      Den totale indsatte værdi
     */
    protected void updateLabels(int cost, int inserted) {
        jLabelLine1.setText(ResourceBundles.getString("payment.line1", cost));
        jLabelLine2.setText(ResourceBundles.getString("payment.line2", inserted));
        jButtonCancel.setText(ResourceBundles.getString("cancel"));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelCoin1 = new javax.swing.JLabel();
        jLabelCoin2 = new javax.swing.JLabel();
        jLabelCoin3 = new javax.swing.JLabel();
        jLabelCoin4 = new javax.swing.JLabel();
        jLabelCoin5 = new javax.swing.JLabel();
        jLabelCoinSlot = new javax.swing.JLabel();
        jPanelDisplay = new javax.swing.JPanel();
        jLabelLine1 = new javax.swing.JLabel();
        jLabelLine2 = new javax.swing.JLabel();
        jButtonCancel = new javax.swing.JButton();

        setBackground(new java.awt.Color(204, 204, 204));

        jPanelDisplay.setBackground(new java.awt.Color(0, 0, 0));
        jPanelDisplay.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Jet Transport", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Simplified Arabic Fixed", 0, 13), new java.awt.Color(255, 255, 255))); // NOI18N

        jLabelLine1.setFont(new java.awt.Font("Simplified Arabic Fixed", 0, 13)); // NOI18N
        jLabelLine1.setForeground(new java.awt.Color(255, 255, 255));
        jLabelLine1.setText("Insert Money");

        jLabelLine2.setFont(new java.awt.Font("Simplified Arabic Fixed", 0, 13)); // NOI18N
        jLabelLine2.setForeground(new java.awt.Color(255, 255, 255));
        jLabelLine2.setText("Inserted Money");

        javax.swing.GroupLayout jPanelDisplayLayout = new javax.swing.GroupLayout(jPanelDisplay);
        jPanelDisplay.setLayout(jPanelDisplayLayout);
        jPanelDisplayLayout.setHorizontalGroup(
            jPanelDisplayLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelDisplayLayout.createSequentialGroup()
                .addComponent(jLabelLine1)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanelDisplayLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelLine2)
                .addContainerGap(207, Short.MAX_VALUE))
        );
        jPanelDisplayLayout.setVerticalGroup(
            jPanelDisplayLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelDisplayLayout.createSequentialGroup()
                .addComponent(jLabelLine1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelLine2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButtonCancel.setText("Cancel");
        jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelDisplay, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonCancel, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelCoinSlot)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelCoin1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelCoin2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelCoin3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelCoin4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelCoin5)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelDisplay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelCoinSlot)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelCoin1)
                    .addComponent(jLabelCoin2)
                    .addComponent(jLabelCoin3)
                    .addComponent(jLabelCoin4)
                    .addComponent(jLabelCoin5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 145, Short.MAX_VALUE)
                .addComponent(jButtonCancel)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelActionPerformed
        cancel = true;
    }//GEN-LAST:event_jButtonCancelActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JLabel jLabelCoin1;
    private javax.swing.JLabel jLabelCoin2;
    private javax.swing.JLabel jLabelCoin3;
    private javax.swing.JLabel jLabelCoin4;
    private javax.swing.JLabel jLabelCoin5;
    private javax.swing.JLabel jLabelCoinSlot;
    private javax.swing.JLabel jLabelLine1;
    private javax.swing.JLabel jLabelLine2;
    private javax.swing.JPanel jPanelDisplay;
    // End of variables declaration//GEN-END:variables
}