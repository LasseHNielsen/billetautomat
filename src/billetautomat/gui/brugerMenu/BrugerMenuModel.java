package billetautomat.gui.brugerMenu;
import billetautomat.data.*;
import billetautomat.gui.brugerMenu.payment.Ticket;
import billetautomat.logger.Log;
import billetautomat.setup.properties.PropertyHandler;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import javax.swing.*;

/**
 * BrugerMenuModel
 * Logik klassen til bruger menu GUI'en
 * @author Lasse Holm Nielsen - S123954
 * @version 26-11-2015
 */
public final class BrugerMenuModel 
{
    private final BrugerMenuView view;                                          //Bruger menu panelet
    private final JLabel[] quantityLabels;
    private int totalCost = 0;                                                  //Den totale pris for de reserverede billeter
    private static final HashMap<Integer, Integer> order = new HashMap();       //Liste for de bestilte billetter
    
    /**
     * Konstruktør for bruger menu logikken
     * @param view 
     *      Bruger menu panelet
     */
    protected BrugerMenuModel(BrugerMenuView view) {
        this.view = view;
        quantityLabels = view.getZoneQuantities();
        updateMessages();
        
        final JLabel[] priceLabels = view.getZonePrices();
        int z = 2;
        for (JLabel priceLabel : priceLabels) {
            priceLabel.setText(""+Data.getPrice(z));
            z++;
        }
    }
    
    /**
     * Tilføjer en billet til bestillingen
     * @param zone
     *      Zone der skal bestilles
     * @param value 
     *      Zonens pris
     */
    protected void addOrder(int zone, int value) {
        int quantity = Integer.parseInt(quantityLabels[zone-2].getText());
        boolean overflow = false;
        
        try {
            Math.addExact(quantity, value);
            Math.subtractExact(quantity, value);
        } catch (ArithmeticException e) {
            overflow = true;
        }
        
        if(!overflow)
            quantity += value;
        
        if(quantity < 0)
            quantity = 0;
        
        quantityLabels[zone-2].setText(""+quantity);
    }
    
    /**
     * Nulstiller alle valgte billeter, så ingen er billeter er bestilt
     */
    protected void resetOrder() {
        for (JLabel quantityLabel : quantityLabels) {
            quantityLabel.setText("0");
        }
        updateOrderTable();
    }
    
    /**
     * Opdaterer teksten i bruger menu GUI'en. Bruges til at skifte sprog
     */
    protected void updateMessages() {
        view.getjLabelTitel().setText(ResourceBundles.getString("brugerMenu.title"));
        view.getjLabelInstructions().setText(ResourceBundles.getString("brugerMenu.instructions"));
        view.getjLabelPriceTitle().setText(ResourceBundles.getString("brugerMenu.priceTitle"));
        view.getjLabelQuantity().setText(ResourceBundles.getString("brugerMenu.QuantityTitle"));
        view.getjLabelOrderTitle().setText(ResourceBundles.getString("brugerMenu.orderTitle"));
        view.getjButtonZoneMap().setText(ResourceBundles.getString("brugerMenu.zoneMapButton"));
        view.getjButtonReset().setText(ResourceBundles.getString("brugerMenu.resetButton"));
        view.getjButtonCancel().setText(ResourceBundles.getString("cancel"));
        view.getjButtonPay().setText(ResourceBundles.getString("brugerMenu.pay"));
        
        int i = 2;
        for (JLabel zoneTitle : view.getZoneTitles()) {
            zoneTitle.setText(ResourceBundles.getString("brugerMenu.zoneTitle", i));
            i++;
        }
        
        updateOrderTable();
    }
    
    /**
     * Beregner hvor mange billeter der er bestilt og af hvilke typer
     */
    private void updateOrder() {
        int z = 2;
        for (JLabel quantityLabel : quantityLabels) {
            final int quantity = Integer.parseInt(quantityLabel.getText());
            order.put(z, quantity);
            z++;
        }
    }
    
    /**
     * Returnere en liste for de bestilte billetter
     * @return 
     *      ordre listen
     */
    protected HashMap<Integer, Integer> getOrder() {
        updateOrder();
        return order;
    }

    /**
     * Opdaterer ordre teksten på bruger menu GUI'en og beregener den totale pris for ordren
     */
    protected void updateOrderTable() {
        final JTextArea table = view.getjTextAreaOrder();
        totalCost = 0;
        updateOrder();
        
        table.setText(ResourceBundles.getString("brugerMenu.orderSubtitle")+"\n");
        for (int zone = 2; zone < 10; zone++) {
            final int quatity = order.get(zone);
            
            if(quatity > 0) {
                final int price = quatity * Data.getPrice(zone);
                totalCost += price;
                table.append("\n"+ResourceBundles.getString("brugerMenu.orderLine", quatity, zone, price));
            }
        }
        table.append("\n\n"+ResourceBundles.getString("brugerMenu.orderTotal", totalCost));
    }

    /**
     * Låser bruger menu GUI'en så brugeren ikke kan foretage sig noget i bruger menuen undtagen at skifte sprog
     * @param freeze 
     *      Sand hvis GUI'en skal begrænset
     */
    protected void freeze(boolean freeze) {
        view.getjButtonZoneMap().setEnabled(!freeze);
        view.getjButtonReset().setEnabled(!freeze);
        view.getjButtonPay().setEnabled(!freeze);
        view.getjButtonCancel().setEnabled(!freeze);
        
        for (JButton button : view.getZoneButtonsAdd()) {
            button.setEnabled(!freeze);
        }
        for (JButton button : view.getZoneButtonsSub()) {
            button.setEnabled(!freeze);
        }
    }

    /**
     * Returnerer prisen for den totale ordre angivet i bruger menu GUI'en
     * @return 
     *      Prisen for alle bestilte billeter
     */
    protected int getTotal() {
        return totalCost;
    }

    /**
     * Printer de bestilte billeter
     */
    protected void printTickets() {
        try {
            updateOrder();
            final String localeInformation = PropertyHandler.get("stationName") + ", zone " + PropertyHandler.get("zone");
            final String date = new SimpleDateFormat("d. MM yyyy HH:mm").format(new Date());
            int price;

            for (int zone = 2; zone < 9; zone++) {
                price = Data.getPrice(zone);

                for (int i = 0; i < order.get(zone); i++) {
                    final JFrame ticket = new JFrame("Jet Transport");
                    ticket.setSize(250, 550);
                    ticket.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    ticket.setContentPane(new Ticket(localeInformation, ""+zone, date, ""+price));
                    ticket.setVisible(true);
                    
                    Log.logger.log(Level.FINE, "{0} zones ticket for {1} printed", new Object[]{zone, price});
                }
            }
        } catch (Exception e) {
            Log.logger.log(Level.SEVERE, "Could not print ticket: {0}", e.toString());
        }
    }
}