package billetautomat.gui.brugerMenu;
import billetautomat.data.*;
import billetautomat.logger.Log;
import billetautomat.setup.properties.PropertyHandler;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.logging.Level;
import javax.swing.*;

/**
 * BrugerMenuView
 * Panel klassen for bruger menu GUI'en
 * @author Lasse Holm Nielsen - S123954
 * @version 26-11-2015
 */
public final class BrugerMenuView extends JPanel 
{
    private final JLabel[] zoneTitles, zonePrices, zoneQuantities;
    private final JButton[] zoneButtonsAdd, zoneButtonsSub;
    
    /**
     * Konstruktør for bruger menu panelet. Sætter font, farver og tekst.
     */
    protected BrugerMenuView() {
        initComponents();
        this.setBackground(GUI.BACKGROUND_COLOR);
        
        //Componenet groups
        zoneTitles = new JLabel[] {jLabelZone2, jLabelZone3, jLabelZone4, jLabelZone5, jLabelZone6, jLabelZone7, jLabelZone8, jLabelZone9};
        zonePrices = new JLabel[] {jLabelPriceZone2, jLabelPriceZone3, jLabelPriceZone4, jLabelPriceZone5, jLabelPriceZone6, jLabelPriceZone7, jLabelPriceZone8, jLabelPriceZone9};
        zoneQuantities = new JLabel[] {jLabelQuanZone2, jLabelQuanZone3, jLabelQuanZone4, jLabelQuanZone5, jLabelQuanZone6, jLabelQuanZone7, jLabelQuanZone8, jLabelQuanZone9};
        zoneButtonsAdd = new JButton[] {jButtonAddZone2, jButtonAddZone3, jButtonAddZone4, jButtonAddZone5, jButtonAddZone6, jButtonAddZone7, jButtonAddZone8, jButtonAddZone9};
        zoneButtonsSub = new JButton[] {jButtonSubZone2, jButtonSubZone3, jButtonSubZone4, jButtonSubZone5, jButtonSubZone6, jButtonSubZone7, jButtonSubZone8, jButtonSubZone9};
        
        //Language flags
        try {
            jLabelDA.setIcon(new ImageIcon(getClass().getResource(Data.IMAGES_FILEPATH + "Denmark-icon.png")));
            jLabelDA.setName("langDa");
            jLabelEN.setIcon(new ImageIcon(getClass().getResource(Data.IMAGES_FILEPATH + "United-Kingdom-icon.png")));
            jLabelEN.setName("langEn");
            jLabelDE.setIcon(new ImageIcon(getClass().getResource(Data.IMAGES_FILEPATH + "Germany-icon.png")));
            jLabelDE.setName("langDe");
            jLabelFR.setIcon(new ImageIcon(getClass().getResource(Data.IMAGES_FILEPATH + "France-icon.png")));
            jLabelFR.setName("langFr");
        } catch (NullPointerException e) {
            Log.logger.log(Level.WARNING, "Could not load flag images: {0}", e.toString());
        }
        
        //Banner
        jLabelInstructions.setBackground(new Color(225, 238, 246));
        jLabelInstructions.setOpaque(true);
        jLabelInstructions.setFont(GUI.HEADER2_FONT);
        jLabelTitel.setFont(GUI.HEADER1_FONT);
        
        //Titles
        jLabelPriceTitle.setFont(GUI.HEADER3_FONT);
        jLabelPriceTitle.setForeground(GUI.FONT_COLOR);
        jLabelQuantity.setFont(GUI.HEADER3_FONT);
        jLabelQuantity.setForeground(GUI.FONT_COLOR);
        jLabelOrderTitle.setFont(GUI.HEADER3_FONT);
        jLabelOrderTitle.setForeground(GUI.FONT_COLOR);
        
        //Components
        Font font = GUI.TEXT_FONT;
        jTextAreaOrder.setFont(font);
        jTextAreaOrder.setBackground(GUI.FONT_COLOR);
        for (JLabel zoneTitle : zoneTitles) {
            zoneTitle.setMinimumSize(new Dimension(-1, Integer.parseInt(PropertyHandler.get("frameHeigth"))/15));
            zoneTitle.setFont(font);
            zoneTitle.setForeground(GUI.FONT_COLOR);
        }
        for (JLabel zonePrice : zonePrices) {
            zonePrice.setFont(font);
            zonePrice.setForeground(GUI.FONT_COLOR);
        }
        for (JLabel zoneQuantity : zoneQuantities) {
            zoneQuantity.setFont(font);
            zoneQuantity.setForeground(GUI.FONT_COLOR);
        }
        
        font = GUI.BUTTON_FONT;
        int z = 2;
        for (JButton zoneButtonAdd : zoneButtonsAdd) {
            zoneButtonAdd.setFont(font);
            zoneButtonAdd.setActionCommand("ADD"+z);
            z++;
        }
        z = 2;
        for (JButton zoneButtonSub : zoneButtonsSub) {
            zoneButtonSub.setFont(font);
            zoneButtonSub.setActionCommand("SUB"+z);
            z++;
        }
        
        jButtonZoneMap.setFont(font);
        jButtonReset.setFont(font);
        jButtonReset.setActionCommand("RESET");
        jButtonCancel.setFont(font);
        jButtonCancel.setActionCommand("CANCEL");
        jButtonPay.setFont(font);
        jButtonPay.setActionCommand("PAY");
    }
    
    /**
     * Tilføjer en mouse listner til flagene
     * @param listener 
     *      Mouse listener for flagene
     */
    protected void addLanguageSelectionListener(MouseListener listener) {
        jLabelDA.addMouseListener(listener);
        jLabelEN.addMouseListener(listener);
        jLabelDE.addMouseListener(listener);
        jLabelFR.addMouseListener(listener);
    }
    
    /**
     * Tilføjer en action listener til kanpperne
     * @param listner 
     *      Action listner for knapperne
     */
    protected void addButtonListner(ActionListener listner) {
        jButtonReset.addActionListener(listner);
        jButtonPay.addActionListener(listner);
        jButtonCancel.addActionListener(listner);
        
        for (JButton zoneButtonAdd : zoneButtonsAdd) {
            zoneButtonAdd.addActionListener(listner);
        }
        for (JButton zoneButtonSub : zoneButtonsSub) {
            zoneButtonSub.addActionListener(listner);
        }
    }

    /**
     * Returnerer label'erne for zonernes navne
     * @return 
     *      Liste med labels for zonernes navne
     */
    protected JLabel[] getZoneTitles() {
        return zoneTitles;
    }

    /**
     * Returnerer knappen "zone map" som bruges til at åbne et panel med et zonekort for hovedstaden (ikke indført endnu)
     * @return 
     *      Knappen "zone map"
     */
    protected JButton getjButtonZoneMap() {
        return jButtonZoneMap;
    }

    /**
     * Returnerer knappen som nulstiller bestillingen
     * @return 
     *      Knappen "reset"
     */
    protected JButton getjButtonReset() {
        return jButtonReset;
    }

    /**
     * Returnerer knappen "Cancel" som afbryder en bestilling og fører brugeren tilbage til startskærmen
     * @return 
     *      Knappen "Cancel"
     */
    protected JButton getjButtonCancel() {
        return jButtonCancel;
    }

    /**
     * Returnerer kanppen "pay" som sender brugeren til betallingsmenuen
     * @return 
     *      Knappen "Pay"
     */
    protected JButton getjButtonPay() {
        return jButtonPay;
    }

    /**
     * Label der instruerer brugeren i hvad brugeren skal forestage sig i menuen
     * @return 
     *      Label der viser instruktioner til brugeren
     */
    protected JLabel getjLabelInstructions() {
        return jLabelInstructions;
    }

    /**
     * Titel label til pris kollonen
     * @return 
     *      Pris titel label
     */
    protected JLabel getjLabelPriceTitle() {
        return jLabelPriceTitle;
    }

    /**
     * Label til antal bestilte billeter af for de forskellige billettyper (koloene 2).
     * @return 
     *      Kvantitets titel label
     */
    protected JLabel getjLabelQuantity() {
        return jLabelQuantity;
    }

    /**
     * Titel label på menuen
     * @return 
     *      Titel label
     */
    protected JLabel getjLabelTitel() {
        return jLabelTitel;
    }

    /**
     * Labels med priserne for de forskellige billettyper
     * @return 
     *      Pris etiketter
     */
    protected JLabel[] getZonePrices() {
        return zonePrices;
    }

    /**
     * Labels der angiver hvor mange billetter af de forskellige billettyper der er bestilt
     * @return 
     *      Antal bestilte billetter for de forskellige typer
     */
    protected JLabel[] getZoneQuantities() {
        return zoneQuantities;
    }

    /**
     * Knapperne der tilføjer bestillingen af en billet
     * @return 
     *      Bestillingsknap
     */
    protected JButton[] getZoneButtonsAdd() {
        return zoneButtonsAdd;
    }

    /**
     * Knapperne der fjerner bestillingen af en billet
     * @return 
     *      Afbestillingsknap
     */
    protected JButton[] getZoneButtonsSub() {
        return zoneButtonsSub;
    }

    /**
     * Titel label for ordre vinduet
     * @return 
     *      Ordre titel label
     */
    public JLabel getjLabelOrderTitle() {
        return jLabelOrderTitle;
    }
    
    /**
     * Bestillings vindue hvor den samlede bestilling vises
     * @return 
     *      Bestillingsvindue
     */
    public JTextArea getjTextAreaOrder() {
        return jTextAreaOrder;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelTitel = new javax.swing.JLabel();
        jLabelInstructions = new javax.swing.JLabel();
        jLabelDA = new javax.swing.JLabel();
        jLabelEN = new javax.swing.JLabel();
        jLabelDE = new javax.swing.JLabel();
        jLabelFR = new javax.swing.JLabel();
        jLabelBlank = new javax.swing.JLabel();
        jLabelZone2 = new javax.swing.JLabel();
        jLabelZone3 = new javax.swing.JLabel();
        jLabelZone4 = new javax.swing.JLabel();
        jLabelZone5 = new javax.swing.JLabel();
        jLabelZone6 = new javax.swing.JLabel();
        jLabelZone7 = new javax.swing.JLabel();
        jLabelZone8 = new javax.swing.JLabel();
        jLabelZone9 = new javax.swing.JLabel();
        jLabelPriceTitle = new javax.swing.JLabel();
        jLabelPriceZone2 = new javax.swing.JLabel();
        jLabelPriceZone3 = new javax.swing.JLabel();
        jLabelPriceZone4 = new javax.swing.JLabel();
        jLabelPriceZone5 = new javax.swing.JLabel();
        jLabelPriceZone6 = new javax.swing.JLabel();
        jLabelPriceZone7 = new javax.swing.JLabel();
        jLabelPriceZone8 = new javax.swing.JLabel();
        jLabelPriceZone9 = new javax.swing.JLabel();
        jLabelQuantity = new javax.swing.JLabel();
        jLabelQuanZone2 = new javax.swing.JLabel();
        jLabelQuanZone3 = new javax.swing.JLabel();
        jLabelQuanZone4 = new javax.swing.JLabel();
        jLabelQuanZone5 = new javax.swing.JLabel();
        jLabelQuanZone6 = new javax.swing.JLabel();
        jLabelQuanZone7 = new javax.swing.JLabel();
        jLabelQuanZone8 = new javax.swing.JLabel();
        jLabelQuanZone9 = new javax.swing.JLabel();
        jLabelOrderTitle = new javax.swing.JLabel();
        jButtonAddZone2 = new javax.swing.JButton();
        jButtonSubZone2 = new javax.swing.JButton();
        jButtonAddZone3 = new javax.swing.JButton();
        jButtonSubZone3 = new javax.swing.JButton();
        jButtonAddZone4 = new javax.swing.JButton();
        jButtonSubZone4 = new javax.swing.JButton();
        jButtonAddZone5 = new javax.swing.JButton();
        jButtonSubZone5 = new javax.swing.JButton();
        jButtonAddZone6 = new javax.swing.JButton();
        jButtonSubZone6 = new javax.swing.JButton();
        jButtonAddZone7 = new javax.swing.JButton();
        jButtonSubZone7 = new javax.swing.JButton();
        jButtonAddZone8 = new javax.swing.JButton();
        jButtonSubZone8 = new javax.swing.JButton();
        jButtonAddZone9 = new javax.swing.JButton();
        jButtonSubZone9 = new javax.swing.JButton();
        jScrollPaneOrder = new javax.swing.JScrollPane();
        jTextAreaOrder = new javax.swing.JTextArea();
        jButtonZoneMap = new javax.swing.JButton();
        jButtonReset = new javax.swing.JButton();
        jButtonPay = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();

        jLabelTitel.setText("Titel");

        jLabelInstructions.setText("Instructions");

        jLabelDA.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N

        jLabelZone2.setText("2 Zones");

        jLabelZone3.setText("3 Zones");

        jLabelZone4.setText("4 Zones");

        jLabelZone5.setText("5 Zones");

        jLabelZone6.setText("6 Zones");

        jLabelZone7.setText("7 Zones");

        jLabelZone8.setText("8 Zones");

        jLabelZone9.setText("All Zones");

        jLabelPriceTitle.setText("Price");

        jLabelPriceZone2.setText("00");

        jLabelPriceZone3.setText("00");

        jLabelPriceZone4.setText("00");

        jLabelPriceZone5.setText("00");

        jLabelPriceZone6.setText("00");

        jLabelPriceZone7.setText("00");

        jLabelPriceZone8.setText("00");

        jLabelPriceZone9.setText("00");

        jLabelQuantity.setText("Amount");

        jLabelQuanZone2.setText("0");

        jLabelQuanZone3.setText("0");

        jLabelQuanZone4.setText("0");

        jLabelQuanZone5.setText("0");

        jLabelQuanZone6.setText("0");

        jLabelQuanZone7.setText("0");

        jLabelQuanZone8.setText("0");

        jLabelQuanZone9.setText("0");

        jLabelOrderTitle.setText("Order");

        jButtonAddZone2.setText("+");

        jButtonSubZone2.setText("-");

        jButtonAddZone3.setText("+");

        jButtonSubZone3.setText("-");

        jButtonAddZone4.setText("+");

        jButtonSubZone4.setText("-");

        jButtonAddZone5.setText("+");

        jButtonSubZone5.setText("-");

        jButtonAddZone6.setText("+");

        jButtonSubZone6.setText("-");

        jButtonAddZone7.setText("+");

        jButtonSubZone7.setText("-");

        jButtonAddZone8.setText("+");

        jButtonSubZone8.setText("-");

        jButtonAddZone9.setText("+");

        jButtonSubZone9.setText("-");

        jTextAreaOrder.setEditable(false);
        jTextAreaOrder.setColumns(20);
        jTextAreaOrder.setRows(5);
        jScrollPaneOrder.setViewportView(jTextAreaOrder);

        jButtonZoneMap.setText("Zone Map");
        jButtonZoneMap.setEnabled(false);

        jButtonReset.setText("Reset");

        jButtonPay.setText("Purchase");

        jButtonCancel.setText("Cancel");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabelTitel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelDA)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelEN)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelDE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelFR))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelInstructions)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabelZone9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelZone8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelZone7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelZone6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelZone5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelZone4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelZone3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelBlank, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelZone2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabelPriceZone5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelPriceTitle, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelPriceZone2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelPriceZone3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelPriceZone4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelPriceZone6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelPriceZone7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelPriceZone8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelPriceZone9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabelQuantity, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelQuanZone2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelQuanZone3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelQuanZone4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelQuanZone5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelQuanZone6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelQuanZone7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelQuanZone8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelQuanZone9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButtonAddZone2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonAddZone3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonAddZone4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonAddZone5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonAddZone6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonAddZone7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonAddZone8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonAddZone9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButtonSubZone2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonSubZone3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonSubZone4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonSubZone5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonSubZone6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonSubZone7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonSubZone8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonSubZone9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPaneOrder)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelOrderTitle)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonZoneMap))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jButtonReset)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 333, Short.MAX_VALUE)
                                .addComponent(jButtonPay)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonCancel)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabelFR)
                        .addComponent(jLabelDE)
                        .addComponent(jLabelEN)
                        .addComponent(jLabelDA))
                    .addComponent(jLabelTitel))
                .addGap(18, 18, 18)
                .addComponent(jLabelInstructions)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelBlank)
                    .addComponent(jLabelPriceTitle)
                    .addComponent(jLabelQuantity)
                    .addComponent(jLabelOrderTitle)
                    .addComponent(jButtonZoneMap))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPaneOrder)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonCancel)
                            .addComponent(jButtonPay)
                            .addComponent(jButtonReset))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelZone2)
                            .addComponent(jLabelPriceZone2)
                            .addComponent(jLabelQuanZone2)
                            .addComponent(jButtonAddZone2)
                            .addComponent(jButtonSubZone2))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelZone3)
                            .addComponent(jLabelPriceZone3)
                            .addComponent(jLabelQuanZone3)
                            .addComponent(jButtonAddZone3)
                            .addComponent(jButtonSubZone3))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelZone4)
                            .addComponent(jLabelPriceZone4)
                            .addComponent(jLabelQuanZone4)
                            .addComponent(jButtonAddZone4)
                            .addComponent(jButtonSubZone4))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelZone5)
                            .addComponent(jLabelPriceZone5)
                            .addComponent(jLabelQuanZone5)
                            .addComponent(jButtonAddZone5)
                            .addComponent(jButtonSubZone5))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelZone6)
                            .addComponent(jLabelPriceZone6)
                            .addComponent(jLabelQuanZone6)
                            .addComponent(jButtonAddZone6)
                            .addComponent(jButtonSubZone6))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelZone7)
                            .addComponent(jLabelPriceZone7)
                            .addComponent(jLabelQuanZone7)
                            .addComponent(jButtonAddZone7)
                            .addComponent(jButtonSubZone7))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelZone8)
                            .addComponent(jLabelPriceZone8)
                            .addComponent(jLabelQuanZone8)
                            .addComponent(jButtonAddZone8)
                            .addComponent(jButtonSubZone8))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelZone9)
                            .addComponent(jLabelPriceZone9)
                            .addComponent(jLabelQuanZone9)
                            .addComponent(jButtonAddZone9)
                            .addComponent(jButtonSubZone9))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAddZone2;
    private javax.swing.JButton jButtonAddZone3;
    private javax.swing.JButton jButtonAddZone4;
    private javax.swing.JButton jButtonAddZone5;
    private javax.swing.JButton jButtonAddZone6;
    private javax.swing.JButton jButtonAddZone7;
    private javax.swing.JButton jButtonAddZone8;
    private javax.swing.JButton jButtonAddZone9;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonPay;
    private javax.swing.JButton jButtonReset;
    private javax.swing.JButton jButtonSubZone2;
    private javax.swing.JButton jButtonSubZone3;
    private javax.swing.JButton jButtonSubZone4;
    private javax.swing.JButton jButtonSubZone5;
    private javax.swing.JButton jButtonSubZone6;
    private javax.swing.JButton jButtonSubZone7;
    private javax.swing.JButton jButtonSubZone8;
    private javax.swing.JButton jButtonSubZone9;
    private javax.swing.JButton jButtonZoneMap;
    private javax.swing.JLabel jLabelBlank;
    private javax.swing.JLabel jLabelDA;
    private javax.swing.JLabel jLabelDE;
    private javax.swing.JLabel jLabelEN;
    private javax.swing.JLabel jLabelFR;
    private javax.swing.JLabel jLabelInstructions;
    private javax.swing.JLabel jLabelOrderTitle;
    private javax.swing.JLabel jLabelPriceTitle;
    private javax.swing.JLabel jLabelPriceZone2;
    private javax.swing.JLabel jLabelPriceZone3;
    private javax.swing.JLabel jLabelPriceZone4;
    private javax.swing.JLabel jLabelPriceZone5;
    private javax.swing.JLabel jLabelPriceZone6;
    private javax.swing.JLabel jLabelPriceZone7;
    private javax.swing.JLabel jLabelPriceZone8;
    private javax.swing.JLabel jLabelPriceZone9;
    private javax.swing.JLabel jLabelQuanZone2;
    private javax.swing.JLabel jLabelQuanZone3;
    private javax.swing.JLabel jLabelQuanZone4;
    private javax.swing.JLabel jLabelQuanZone5;
    private javax.swing.JLabel jLabelQuanZone6;
    private javax.swing.JLabel jLabelQuanZone7;
    private javax.swing.JLabel jLabelQuanZone8;
    private javax.swing.JLabel jLabelQuanZone9;
    private javax.swing.JLabel jLabelQuantity;
    private javax.swing.JLabel jLabelTitel;
    private javax.swing.JLabel jLabelZone2;
    private javax.swing.JLabel jLabelZone3;
    private javax.swing.JLabel jLabelZone4;
    private javax.swing.JLabel jLabelZone5;
    private javax.swing.JLabel jLabelZone6;
    private javax.swing.JLabel jLabelZone7;
    private javax.swing.JLabel jLabelZone8;
    private javax.swing.JLabel jLabelZone9;
    private javax.swing.JScrollPane jScrollPaneOrder;
    private javax.swing.JTextArea jTextAreaOrder;
    // End of variables declaration//GEN-END:variables

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        final BufferedImage logo = GUI.BANNER;
        g.drawImage(logo, 0, 0, null);
    }
}