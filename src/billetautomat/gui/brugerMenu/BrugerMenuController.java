package billetautomat.gui.brugerMenu;
import billetautomat.listeners.LanguageChosenListener;
import billetautomat.listeners.BrugerMenuButtonListener;
import java.util.HashMap;

/**
 * BrugerMenuController
 * Controller til Bruger menu GUI'en
 * @author Lasse Holm Nielsen - S123954
 * @version 26-11-2015
 */
public class BrugerMenuController 
{    
    private final BrugerMenuView view;                                          //Bruger menu panelet
    private static BrugerMenuModel model;                                       //Logik klassen til bruger menuen

    /**
     * Konstruktør til bruger menu controlleren
     */
    public BrugerMenuController() {
        view = new BrugerMenuView();
        model = new BrugerMenuModel(view);
        
        view.addLanguageSelectionListener(new LanguageChosenListener());
        view.addButtonListner(new BrugerMenuButtonListener());
    }
    
    /**
     * Returnerer bruger menu panelet
     * @return 
     *      Bruger menu panelet
     */
    public BrugerMenuView getView() {
        return view;
    }
    
    /**
     * Returnerer prisen for den totale ordre angivet i bruger menu GUI'en
     * @return 
     *      Prisen for alle bestilte billeter
     */
    public static int getCost() {
        return model.getTotal();
    }
    
    /**
     * Returnere en liste for de bestilte billetter
     * @return 
     *      ordre listen
     */
    public static HashMap<Integer, Integer> getOrder() {
        return model.getOrder();
    }
    
    /**
     * Opdaterer teksten i bruger menu GUI'en. Bruges til at skifte sprog
     */
    public static void updateMessages() {
        model.updateMessages();
    }
    
    /**
     * Nulstiller alle valgte billeter, så ingen er billeter er bestilt
     */
    public static void resetOrder() {
        model.resetOrder();
    }
    
    /**
     * Tilføjer en billet til bestillingen
     * @param zone
     *      Zone der skal bestilles
     * @param value 
     *      Zonens pris
     */
    public static void addOrder(int zone, int value) {
        model.addOrder(zone, value);
        model.updateOrderTable();
    }
    
    /**
     * Låser bruger menu GUI'en så brugeren ikke kan foretage sig noget i bruger menuen undtagen at skifte sprog
     * @param freeze 
     *      Sand hvis GUI'en skal begrænset
     */
    public static void freezeScreen(boolean freeze) {
        model.freeze(freeze);
    }

    /**
     * Printer de bestilte billeter
     */
    public static void printTickets() {
        model.printTickets();
    }
}